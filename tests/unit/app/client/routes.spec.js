import routes from 'App/client/routes';

describe('ClientRoutes', () => {
  it('should a test feature routes', async () => {
    const message = `Rota: ${routes[0].path}`;

    expect(routes[0].path, message).toBe('clients');
    expect(routes[0].component, message).toBeTruthy();
    expect(routes[0]).toHaveProperty('name');
  });
});
