import ClientEditPage from 'App/client/ClientEditPage.vue';
import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import LojaCrudList from 'App/client/lojas/LojaCrudList.vue';
import PageTemplate from 'Arch/template/PageTemplate.vue';
import LojaForm from 'App/client/lojas/LojaForm.vue';
import PageTitle from 'Arch/template/PageTitle.vue';
import ClientForm from 'App/client/ClientForm.vue';
import HeaderComponent from 'App/client/components/HeaderComponent.vue';
import Vue from 'vue';

const factory = (mocks) => createWrapper(
  ClientEditPage,
  mocks,
  ['v-lazy', 'loja-crud-list'],
);

describe('ClientEditPage', () => {
  it('should be able a vue instance', async () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a render basic html components', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .find(PageTemplate, 1)
      .find(HeaderComponent, 1)
      .find(PageTitle, 1);
  });

  it('should be able a render client form', async () => {
    const wrapper = factory({
      $route: {
        params: {
          id: 1,
        },
      },
    });

    wrapper.vm.tabs = 0;

    await Vue.nextTick();
    await Vue.nextTick();

    testWrapper(wrapper)
      .find(ClientForm, 1);
  });

  it('should be able a render loja list', async () => {
    const wrapper = factory({
      $route: {
        params: {
          id: 1,
        },
      },
    });

    wrapper.vm.tabs = 1;

    await Vue.nextTick();
    await Vue.nextTick();

    testWrapper(wrapper)
      .find(LojaCrudList, 1);
  });

  it('should be able a render loja form', async () => {
    const wrapper = factory({
      $route: {
        params: {
          id: 1,
        },
      },
    });

    wrapper.vm.tabs = 1;
    wrapper.vm.newStore = true;

    await Vue.nextTick();
    await Vue.nextTick();

    testWrapper(wrapper)
      .find(LojaForm, 1);
  });
});
