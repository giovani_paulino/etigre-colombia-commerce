import testCrudRequest from '@test/helpers/testCrudRequest';
import ClientRequest from 'App/client/ClientRequest';

describe('ClientRequest', () => {
  it('should a return promise on call all methods', async () => {
    testCrudRequest(ClientRequest);
  });
});
