import testCrudList from '@test/helpers/testCrudList';
import ClientCrudList from 'App/client/ClientCrudList.vue';
import ClientRequest from 'App/client/ClientRequest';
import DataTableDateColumn from 'Arch/components/data-table/DataTableColumns/DataTableDateColumn.vue';
import DataTableTextColumn from 'Arch/components/data-table/DataTableColumns/DataTableTextColumn.vue';
import DataTableCpfCnpjColumn from 'Arch/components/data-table/DataTableColumns/DataTableCpfCnpjColumn.vue';
import StatusDataTableColumnVue from 'App/client/components/table/column/StatusDataTableColumn.vue';

jest.mock('App/client/ClientRequest', () => ({
  list: jest.fn(),
  remove: jest.fn(),
}));

const mockListResolvedDataValue = [
  {
    nomCliente: 'hadson',
    indStatus: 'A',
    cpfCnpj: '12345678912345',
    tipoCliente: 'PJ',
    numTelefone: '12345678',
    txtEmail: 'kjfoahfo',
    updatedDate: new Date(),
  },
];

describe('ClientCrudList', () => {
  it('should a test all features', async () => {
    const wrapper = await testCrudList({
      componentCrudList: ClientCrudList,
      listResolvedDataValueMock: mockListResolvedDataValue,
      requestClassMock: ClientRequest,
    });

    const testColumn = (wrapperTest, { column, length }) => {
      const columns = wrapperTest.findAllComponents(column);
      expect(columns).toHaveLength(length);
      Array(length).forEach((_, i) => expect(columns.at(i).exists()).toBeTruthy());
    };

    testColumn(wrapper, { column: DataTableTextColumn, length: 4 });
    testColumn(wrapper, { column: DataTableDateColumn, length: 1 });
    testColumn(wrapper, { column: DataTableCpfCnpjColumn, length: 1 });
    testColumn(wrapper, { column: StatusDataTableColumnVue, length: 1 });
  });
});
