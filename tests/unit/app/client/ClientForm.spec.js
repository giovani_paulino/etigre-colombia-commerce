import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import ClientForm from 'App/client/ClientForm.vue';
import CreateClientcontent from 'App/client/components/form/CreateClientContent.vue';
import EditClientContent from 'App/client/components/form/EditClientContent.vue';
import CrudForm from 'Arch/components/crud/CrudForm.vue';

const mockInputsValue = {
  cpfCnpj: '12345678978',
  numRgInscricaoEstadual: 123456789,
  orgaoEmissor: 'SSP',
  nomCliente: 'Thalison',
  datNascimento: '11991999',
  dscMotivoBloqueio: 'lkasdhfhjashsdfja',
  idEmpresa: 1,
  idEndereco: 2,
  idUsuarioAdmin: 3,
  indBloqueado: true,
  indDesbloqueioAutomatico: true,
  indStatus: 'A',
  nomMae: 'dihfioadhjf',
  nomRazaoSocial: 'dfhajdhfjkahdf',
  numEndereco: 123,
  numTelefone: 5511912345678,
  qtdDiasBloqueioInadimplencia: 0,
  tipoCliente: 'PF',
  txtComplementoEndereco: 'apsjfoçakdjsfkaj',
  txtEmail: 'adfhjjaldhfjkah',
  txtObservacao: 'dfhjjkadhfajhdj',
  ufEmissao: 'SP',
  vlrLimiteCredito: 500,
  enderecoOutputJO: {
    bairroEnd: 'Test',
    cidadeEnd: 'test',
    estadoEnd: 'SP',
    idEndereco: 6,
    logradEnd: 'hi people',
    numCep: '05065010',
  },
};

const mockResolvedValue = {
  data: mockInputsValue,
};

const factory = (mocks) => createWrapper(ClientForm, mocks);

describe('ClientForm', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content client create inputs', () => {
    const wrapper = factory();

    testWrapper(wrapper).find(CreateClientcontent, 1);
  });

  it('should be able a content client edit inputs', () => {
    const wrapper = factory({
      $route: {
        params: {
          id: 1,
        },
      },
    });

    testWrapper(wrapper).find(EditClientContent, 1);
  });

  it('should be able a create new client', async () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValue;
    const form = wrapper.find(CrudForm);

    testWrapper(form);

    const spyRequest = jest.spyOn(wrapper.vm.requestClass, 'save')
      .mockResolvedValue(mockResolvedValue);

    form.vm.create();

    expect(spyRequest).toBeCalledTimes(1);
    await expect(wrapper.vm.requestClass.save())
      .resolves.toEqual(mockResolvedValue);
  });

  it('should be able a edit a client', async () => {
    const wrapper = factory({
      mocks: {
        $route: {
          params: { id: 1 },
        },
      },
    });

    const spyRequest = jest.spyOn(wrapper.vm.requestClass, 'edit')
      .mockResolvedValue(mockResolvedValue);

    wrapper.vm.payload = mockInputsValue;

    const form = wrapper.find(CrudForm);

    testWrapper(form);

    form.vm.update();

    expect(spyRequest).toBeCalledTimes(1);
    await expect(wrapper.vm.requestClass.edit())
      .resolves.toEqual(mockResolvedValue);
  });
});
