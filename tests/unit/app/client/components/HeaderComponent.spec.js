import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import HeaderComponent from 'App/client/components/HeaderComponent.vue';
import ChipFinancial from '@/app/client/components/chips/ChipFinancial.vue';
import ChipStatus from '@/app/client/components/chips/ChipStatus.vue';

const mockDataValue = {
  id: 1,
  nomRazaoSocial: 'test',
  cpfCnpj: '12345678978',
  tipoCliente: 'PJ',
  indStatus: 'A',
  indBloqueado: true,
};

const mockResolvedValue = {
  data: mockDataValue,
};

const factory = () => createWrapper(
  HeaderComponent,
  {
    $route: {
      params: {
        id: 1,
      },
    },
  },
  undefined,
);

describe('HeaderComponent', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockDataValue;

    testWrapper(wrapper)
      .find(ChipFinancial, 1)
      .find(ChipStatus, 1);
  });

  it('should be able a request client data', async () => {
    const wrapper = factory();

    const spyRequest = jest.spyOn(wrapper.vm.requestClass, 'get')
      .mockResolvedValue(mockResolvedValue);

    wrapper.vm.getDataContent();

    expect(spyRequest).toBeCalled();
    await expect(wrapper.vm.requestClass.get())
      .resolves.toEqual(mockResolvedValue);
  });
});
