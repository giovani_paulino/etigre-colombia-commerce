import ClientFilter from '@/app/client/components/table/filters/ClientFilter.vue';
import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import FilterTemplate from '@/app/arch/components/crud/components/FilterTemplate.vue';

const mockResolvedDataValue = {
  id: '',
  cpfCnpj: '',
  nomCliente: '',
  company: '',
  region: '',
  moneySituation: 'string',
  registerSituation: 'string',
  saleSituation: 'string',
  paymentModalit: 'pre-pago',
  tipoCliente: '',
  clasificacao: '',
  saleSysten: '',
  area: '',
  ownerSale: '',
  city: '',
  state: '',
  neigborhood: '',
  saleConsult: '',
};

const div = document.createElement('div');
document.body.appendChild(div);

const factory = (mocks) => createWrapper(ClientFilter, mocks);

describe('ClientFilter', () => {
  it('should be able a render html component', async () => {
    const wrapper = factory({ $route: { query: { page: 1 } } });

    wrapper.vm.payload = mockResolvedDataValue;
    testWrapper(wrapper).find(FilterTemplate, 1);
  });
});
