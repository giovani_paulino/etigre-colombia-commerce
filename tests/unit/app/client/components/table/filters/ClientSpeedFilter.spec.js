import ClientSpeedFilter from '@/app/client/components/table/filters/ClientSpeedFilter.vue';
import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import FilterSpeedTemplate from '@/app/arch/components/crud/components/FilterSpeedTemplate.vue';

const mockResolvedValue = {
  adiplentes: true,
  ativos: true,
  cnpj: true,
  pj: true,
};

describe('ClientSpeedFilter', () => {
  const wrapper = createWrapper(ClientSpeedFilter, { $route: { query: { page: 1 } } });

  it('should be able a vue instance', async () => {
    testWrapper(wrapper);
  });

  it('should be able a render html', () => {
    wrapper.vm.payload = mockResolvedValue;

    testWrapper(wrapper)
      .find(FilterSpeedTemplate, 1)
      .find({ name: 'VCheckbox' }, 4);
  });
});
