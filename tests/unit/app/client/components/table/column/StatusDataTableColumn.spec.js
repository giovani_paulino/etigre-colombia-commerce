import StatusDataTableColumn from '@/app/client/components/table/column/StatusDataTableColumn.vue';
import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import ChipStatus from '@/app/client/components/chips/ChipStatus.vue';

const wrapper = createWrapper(StatusDataTableColumn, undefined, undefined, {
  propsData: {
    value: 'I',
  },
});

describe('StatusDataTableColumn', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be able a render html components', () => {
    wrapper.vm.value = 'I';

    testWrapper(wrapper)
      .find(ChipStatus, 1, (chip) => {
        expect(chip.text()).toBe('Inativo');
      });
  });
});
