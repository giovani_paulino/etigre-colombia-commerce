import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import CustomButton from 'App/client/components/form/buttons/CustomButton.vue';

const wrapper = createWrapper(CustomButton);

const mockHandleClick = jest.fn();

describe('CustomButton', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    wrapper.setProps({
      text: 'test',
      onClick: mockHandleClick,
    });

    expect(wrapper.vm.text).toEqual('test');

    testWrapper(wrapper).find({ name: 'button' }, 1);
  });
});
