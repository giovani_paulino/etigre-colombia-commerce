import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import SubmitButton from 'App/client/components/form/buttons/SubmitButton.vue';

const wrapper = createWrapper(SubmitButton);

describe('SubmitButton', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    wrapper.setProps({
      text: 'success',
      loading: true,
    });

    expect(wrapper.vm.text).toEqual('success');
    expect(wrapper.vm.loading).toBeTruthy();
  });
});
