import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import FormButtons from 'App/client/components/form/buttons/FormButtons.vue';
import SubmitButton from 'App/client/components/form/buttons/SubmitButton.vue';
import CustomButton from 'App/client/components/form/buttons/CustomButton.vue';

const factory = (props) => createWrapper(FormButtons, undefined, undefined, props);

const handleClick = jest.fn();
const navigateTo = jest.fn();

describe('FormButtons', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', async () => {
    const wrapper = factory({
      propsData: {
        nextButton: true,
        handleClick,
        navigateTo,
      },
    });

    wrapper.vm.nextButton = true;
    wrapper.vm.handleClick = handleClick;
    wrapper.vm.navigateTo = navigateTo;

    await wrapper.vm.$nextTick();

    expect(wrapper.vm.nextButton).toBeTruthy();
    expect(wrapper.vm.handleClick).toBeTruthy();
    expect(wrapper.vm.navigateTo).toBeTruthy();

    wrapper.destroy();
  });

  it('should be able a render submit button', () => {
    const wrapper = factory({
      propsData: {
        nextButton: false,
        handleClick,
        navigateTo,
      },
    });

    testWrapper(wrapper).find(SubmitButton, 1);

    wrapper.destroy();
  });

  it('should be able a render custom button', async () => {
    const wrapper = factory({
      propsData: {
        nextButton: true,
        handleClick,
        navigateTo,
      },
    });

    wrapper.vm.nextButton = true;
    wrapper.vm.handleClick = handleClick;
    wrapper.vm.navigateTo = navigateTo;

    await wrapper.vm.$nextTick();

    testWrapper(wrapper).find(CustomButton, 1);

    wrapper.destroy();
  });
});
