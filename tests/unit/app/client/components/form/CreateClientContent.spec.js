import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import CreateClientContent from 'App/client/components/form/CreateClientContent.vue';
import FormGroup from '@/app/arch/components/crud/components/FormGroup.vue';
import ClientContactInputs from 'App/client/components/form/inputs/group/ClientContactInputs.vue';
import ClientEndInputs from 'App/client/components/form/inputs/group/ClientEndInputs.vue';
import ClientAccessInputs from 'App/client/components/form/inputs/group/ClientAccessInputs.vue';
import FormButtons from 'App/client/components/form/buttons/FormButtons.vue';
import ClientDataContentInputs from 'App/client/components/form/inputs/group/ClientDataContentInputs.vue';

const mockInputsValues = {
  indBloqueado: true,
  vlrLimiteCredito: 100,
  indDesbloqueioAutomatico: false,
  qtdDiasBloqueioInadimplencia: 1,
  dscMotivoBloqueio: 'não passou no test',
  numTelefone: '5511912345678',
  txtEmail: 'test@example.com.br',
  nomCliente: 'test client',
  datNascimento: '11-11-9999',
  nomMae: 'mae test',
  cpfCnpj: '12345678978',
  numRgInscricaoEstadual: '11111111111',
  orgaoEmissor: 'SSP',
  ufEmissao: 'JS',
  nomRazaoSocial: 'test store',
  tipoCliente: 'PJ',
  idEmpresa: 1,
  txtObservacao: 'test observation',
  enderecoOutputJO: {
    numCep: 76890000,
    logradEnd: 'Rua das abóbrinhas',
    numEndereco: 400,
    bairroEnd: 'Bairro dos loucos',
    cidadeEnd: 'Cidade invisivel',
    estadoEnd: 'Sp',
  },
  txtComplementoEndereco: 'Não tinha teto',
  idEndereco: 1,
};

const mocksPropsData = {
  payload: mockInputsValues,
};

const factory = () => createWrapper(CreateClientContent, undefined, undefined, {
  propsData: {
    ...mocksPropsData,
  },
});

describe('CreateClientContent', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a render basic html components', () => {
    const wrapper = factory();
    testWrapper(wrapper)
      .find(FormGroup, 5)
      .find(FormButtons, 4)
      .find(ClientDataContentInputs, 1)
      .find(ClientEndInputs, 1)
      .find(ClientAccessInputs, 1)
      .find(ClientContactInputs, 1)
      .find({ name: 'VStepper' }, 1);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();
    testWrapper(wrapper)
      .toContain(mocksPropsData);
  });
});
