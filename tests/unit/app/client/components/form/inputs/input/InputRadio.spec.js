import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import InputRadio from 'App/client/components/form/inputs/input/InputRadio.vue';

const mockPropsData = {
  options: [],
  row: true,
  disabled: false,
  rules: [],
};

const factory = (value = '') => createWrapper(InputRadio, undefined, undefined, {
  propsData: {
    value,
    ...mockPropsData,
  },
});

describe('InputRadio', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper).toContain(mockPropsData);
  });

  it('should be able a set value', () => {
    const wrapper = factory();

    wrapper.vm.value = '1';

    expect(wrapper.vm.radio).toBe('1');
  });

  it('should be able a verify input event', () => {
    const wrapper = factory();

    wrapper.vm.value = '1';
    wrapper.vm.radio = '2';

    expect(wrapper.emitted().input[0][0]).toEqual('2');
  });
});
