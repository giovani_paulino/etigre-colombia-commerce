import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import InputEdit from 'App/client/components/form/inputs/input/InputEdit.vue';

const mockPropsData = {
  conditional: true,
  handleDisable: jest.fn(),
  btnText: '',
  onClickBtn: jest.fn(),
};

const mockSlotInput = {
  name: 'input',
  template: '<p>test input</p>',
};

const mockSlotDisabled = {
  name: 'disabled',
  template: '<p>test disabled</p>',
};

const mockSlotProps = {
  input: mockSlotInput,
  disabled: mockSlotDisabled,
};

const factory = () => createWrapper(InputEdit, undefined, undefined, {
  propsData: {
    ...mockPropsData,
  },
  slots: {
    ...mockSlotProps,
    default: [],
  },
});

describe('InputEdit', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .toContain(mockPropsData)
      .contains(mockSlotProps);
  });
});
