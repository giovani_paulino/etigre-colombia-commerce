import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import DataList from 'App/client/components/form/inputs/input/DataList.vue';

const mockDataList = [
  'test',
  'test 1',
];

const wrapper = createWrapper(DataList, undefined, undefined, {
  propsData: {
    list: mockDataList,
  },
});

describe('DataList', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be a content basic props', () => {
    wrapper.vm.list = mockDataList;

    expect(wrapper.vm.list).toEqual(mockDataList);
  });
});
