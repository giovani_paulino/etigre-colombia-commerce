import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import ClientDataContentInputs from 'App/client/components/form/inputs/group/ClientDataContentInputs.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import InputSelect from 'Arch/components/global-components/InputSelect.vue';
import { multipleInput } from '@test/helpers/inputTestHelper';

const mockValueInputs = {
  nomCliente: 'test client',
  datNascimento: '11-11-9999',
  nomMae: 'mae test',
  cpfCnpj: '12345678978',
  numRgInscricaoEstadual: '11111111111',
  orgaoEmissor: 'SSP',
  ufEmissao: 'JS',
  nomRazaoSocial: 'test store',
  tipoCliente: 'PJ',
  idEmpresa: 1,
  txtObservacao: 'test observation',
};

const mockPropsData = {
  payload: mockValueInputs,
};

const wrapper = createWrapper(ClientDataContentInputs, undefined, undefined, {
  propsData: {
    ...mockPropsData,
  },
});

const inputs = createEqualPayloadInputs(mockValueInputs);

describe('ClientDataContentInputs', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    testWrapper(wrapper)
      .find(InputSelect, 3)
      .find(InputText, 8, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate inputs', () => {
    multipleInput(wrapper, inputs);
  });
});
