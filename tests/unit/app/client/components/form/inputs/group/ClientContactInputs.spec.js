import { multipleInput } from '@test/helpers/inputTestHelper';
import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import ClientContactInputs from 'App/client/components/form/inputs/group/ClientContactInputs.vue';
import InputText from 'Arch/components/global-components/InputText.vue';

const mockInputsValues = {
  numTelefone: '5511912345678',
  txtEmail: 'test@example.com.br',
};

const mockPropsData = {
  payload: mockInputsValues,
};

const wrapper = createWrapper(ClientContactInputs, undefined, undefined, {
  propsData: {
    ...mockPropsData,
  },
});

const inputs = createEqualPayloadInputs(mockInputsValues);

describe('ClientContactInputs', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be able a content basica props', () => {
    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    testWrapper(wrapper)
      .find(InputText, 2, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate input values', () => {
    multipleInput(wrapper, inputs);
  });
});
