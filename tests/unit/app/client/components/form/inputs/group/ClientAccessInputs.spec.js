import ClientAccessInputs from 'App/client/components/form/inputs/group/ClientAccessInputs.vue';
import FormGroup from '@/app/arch/components/crud/components/FormGroup.vue';
import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import InputRadio from 'App/client/components/form/inputs/input/InputRadio.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import { multipleInput } from '@test/helpers/inputTestHelper';

const mockInputsValue = {
  indBloqueado: true,
  vlrLimiteCredito: 100,
  indDesbloqueioAutomatico: false,
  qtdDiasBloqueioInadimplencia: 1,
  dscMotivoBloqueio: 'não passou no test',
};

const mockPropsData = {
  payload: mockInputsValue,
};

const wrapper = createWrapper(ClientAccessInputs, undefined, undefined, {
  propsData: {
    ...mockPropsData,
  },
});

const inputs = createEqualPayloadInputs(mockInputsValue);

describe('ClientAccessInputs', () => {
  it('should be able a vue instance', () => {
    testWrapper(wrapper);
  });

  it('should be able a contain basic props', () => {
    testWrapper(wrapper).toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    testWrapper(wrapper)
      .find(FormGroup, 1)
      .find(InputRadio, 2)
      .find(InputText, 3, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate inputs value', () => {
    multipleInput(wrapper, inputs);
  });
});
