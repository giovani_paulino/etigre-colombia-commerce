import ClientRequest from '@/app/client/ClientRequest';
import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import ClientEndInputs from 'App/client/components/form/inputs/group/ClientEndInputs.vue';
import FormGroup from '@/app/arch/components/crud/components/FormGroup.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import CustomButton from 'App/client/components/form/buttons/CustomButton.vue';
import { multipleInput } from '@test/helpers/inputTestHelper';

const mockInputsValues = {
  enderecoOutputJO: {
    numCep: 76890000,
    logradEnd: 'Rua das abóbrinhas',
    numEndereco: 400,
    bairroEnd: 'Bairro dos loucos',
    cidadeEnd: 'Cidade invisivel',
    estadoEnd: 'Sp',
  },
  txtComplementoEndereco: 'Não tinha teto',
  idEndereco: 1,
};

const mockPropsData = {
  payload: mockInputsValues,
};

const factory = () => createWrapper(ClientEndInputs, {
  $swal: jest.fn(),
}, undefined, {
  propsData: {
    ...mockPropsData,
  },
});

const inputs = [
  ...createEqualPayloadInputs(mockInputsValues, 'idEndereco', 'enderecoOutputJO'),
  ...createEqualPayloadInputs(mockInputsValues.enderecoOutputJO),
];

jest.mock('@/app/client/ClientRequest', () => ({
  getEndereco: jest.fn().mockResolvedValue({
    data: {
      total: 1,
      data: [
        {
          id: 1,
          logradouro: 'Rua das abóbrinhas',
          bairro: 'Bairro dos loucos',
          localidade: 'Cidade invisivel',
          uf: 'Sp',
        },
      ],
    },
  }),
}));

describe('ClientEndInputs', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    testWrapper(wrapper)
      .toContain({
        ...mockPropsData,
        requestClass: ClientRequest,
        inputsDisabled: true,
      });
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    testWrapper(wrapper)
      .find(FormGroup, 1)
      .find(CustomButton, 1)
      .find(InputText, 7, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate inputs value', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    multipleInput(wrapper, inputs);
  });

  it('should be able a render dataList options', () => {
    const wrapper = factory();

    wrapper.vm.inputsDisabled = false;
    wrapper.vm.logradEndList = ['test', 'tes1'];
    wrapper.vm.bairroEndList = ['test', 'tes1'];

    const bairroEndList = wrapper.find('[id="bairroEndList"]');
    const logradEndList = wrapper.find('[id="logradEndList"]');

    expect(bairroEndList).toBeTruthy();
    expect(logradEndList).toBeTruthy();
  });

  it('should be able a consulting cpf', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    testWrapper(wrapper)
      .find(CustomButton, 1);

    wrapper.vm.serchCep();

    expect(ClientRequest.getEndereco).toBeCalledTimes(1);
  });
});
