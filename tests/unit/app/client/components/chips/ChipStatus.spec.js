import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import ChipStatus from '@/app/client/components/chips/ChipStatus.vue';

const factory = (status) => createWrapper(ChipStatus, undefined, undefined, {
  propsData: {
    status,
  },
});

describe('ChipStatus', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();
    testWrapper(wrapper);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();
    testWrapper(wrapper)
      .find({ name: 'VChip' }, 1, (chip) => {
        expect(chip.text()).toBeTruthy();
      });
  });

  it('should be able a equal active status', async () => {
    const wrapper = factory('A');
    wrapper.vm.status = 'A';

    expect(wrapper.vm.getStatus).toEqual({
      color: '#EAFAF5',
      textColor: 'success',
      label: 'Ativo',
    });
  });

  it('should be able a equal inative status', () => {
    const wrapper = factory('I');
    wrapper.vm.status = 'I';

    expect(wrapper.vm.getStatus).toEqual({
      color: '#FAEAEB',
      textColor: '#8C8282',
      label: 'Inativo',
    });
  });
});
