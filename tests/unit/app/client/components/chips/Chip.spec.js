import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import Chip from 'App/client/components/chips/Chip.vue';

const mockProps = {
  color: '#ff0',
  textColor: '#fff',
  label: 'test',
};

const mockPropsData = {
  payload: mockProps,
};

const factory = () => createWrapper(
  Chip,
  undefined,
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
  },
);

describe('Chip', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockProps;

    testWrapper(wrapper)
      .find({ name: 'VChip' }, 1);

    expect(wrapper.vm.payload).toEqual(mockProps);
  });
});
