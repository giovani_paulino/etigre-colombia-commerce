import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import ChipFinancial from '@/app/client/components/chips/ChipFinancial.vue';

const factory = (status) => createWrapper(ChipFinancial, undefined, undefined, {
  propsData: {
    status,
  },
});

describe('ChipFinancial', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();
    testWrapper(wrapper);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();
    testWrapper(wrapper)
      .find({ name: 'VChip' }, 1, (chip) => {
        expect(chip.text()).toBeTruthy();
      });
  });

  it('should be able a equal locked status', async () => {
    const wrapper = factory(true);
    wrapper.vm.status = true;

    expect(wrapper.vm.getStatus).toEqual({
      color: '#EAFAF5',
      textColor: 'success',
      label: 'Bloqueado',
    });
  });

  it('should be able a equal unlocked status', () => {
    const wrapper = factory(false);
    wrapper.vm.status = false;

    expect(wrapper.vm.getStatus).toEqual({
      color: '#FAEAEB',
      textColor: '#8C8282',
      label: 'Desbroqueado',
    });
  });
});
