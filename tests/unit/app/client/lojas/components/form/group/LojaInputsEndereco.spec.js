import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import LojaInputsEndereco from 'App/client/lojas/components/form/group/LojaInputsEndereco.vue';
import FormGroup from '@/app/arch/components/crud/components/FormGroup.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import CustomButton from 'App/client/components/form/buttons/CustomButton.vue';
import { multipleInput } from '@test/helpers/inputTestHelper';

const mockInputsValue = {
  enderecoOutputJO: {
    numCep: 76890000,
    logradEnd: 'Rua das abóbrinhas',
    numEndereco: 400,
    bairroEnd: 'Bairro dos loucos',
    cidadeEnd: 'Cidade invisivel',
    estadoEnd: 'Sp',
  },
  txtComplementoEndereco: 'Não tinha teto',
  idEndereco: 1,

};

const mockPropsData = {
  payload: mockInputsValue,
};

const inputs = [
  ...createEqualPayloadInputs(mockInputsValue, 'idEndereco', 'enderecoOutputJO'),
  ...createEqualPayloadInputs(mockInputsValue.enderecoOutputJO),
];

const factory = () => createWrapper(
  LojaInputsEndereco,
  {
    $swal: jest.fn(),
  },
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
  },
);

const mockResolvedValue = {
  data: {
    total: 1,
    data: [
      {
        id: 1,
        logradouro: 'Rua das abóbrinhas',
        bairro: 'Bairro dos loucos',
        localidade: 'Cidade invisivel',
        uf: 'Sp',
      },
    ],
  },
};

describe('LojaInputsEndereco', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();
    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockPropsData.payload.idEndereco;

    testWrapper(wrapper)
      .toContain(mockPropsData);

    expect(wrapper.vm.requestClass).toBeTruthy();
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockPropsData.payload;

    testWrapper(wrapper)
      .find(FormGroup, 1)
      .find(CustomButton, 1)
      .find(InputText, 7, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate all inputs', () => {
    const wrapper = factory();
    wrapper.vm.payload = mockPropsData.payload;

    multipleInput(wrapper, inputs);
  });

  it('should be able a render dataList options', () => {
    const wrapper = factory();

    wrapper.vm.inputsDisabled = false;
    wrapper.vm.logradEndList = ['test', 'tes1'];
    wrapper.vm.bairroEndList = ['test', 'tes1'];

    const bairroEndList = wrapper.find('[id="bairroEndList"]');
    const logradEndList = wrapper.find('[id="logradEndList"]');

    expect(bairroEndList).toBeTruthy();
    expect(logradEndList).toBeTruthy();
  });

  it('should be able posible consulting cep', async () => {
    const wrapper = factory();

    wrapper.vm.payload = mockPropsData.payload;

    const spySearchCep = jest.spyOn(wrapper.vm.requestClass, 'getEndereco')
      .mockResolvedValue(mockResolvedValue);

    testWrapper(wrapper)
      .find(CustomButton, 1);

    wrapper.vm.serchCep();

    expect(spySearchCep).toBeCalledTimes(1);
    await expect(wrapper.vm.requestClass.getEndereco())
      .resolves.toEqual(mockResolvedValue);
  });
});
