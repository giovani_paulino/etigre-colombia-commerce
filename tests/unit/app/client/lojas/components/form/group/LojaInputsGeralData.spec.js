import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import LojaInputsGeralData from 'App/client/lojas/components/form/group/LojaInputsGeralData.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import InputSelect from 'Arch/components/global-components/InputSelect.vue';
import { multipleInput } from '@test/helpers/inputTestHelper';

const mocksInputsValues = {
  idSegmento: 1,
  idTemplate: 2,
  indStatus: 'A',
  nomLoja: 'Dev test store',
  numCnpj: 12345678978945,
};

const mockPropsData = {
  payload: mocksInputsValues,
};

const inputs = createEqualPayloadInputs(
  mocksInputsValues,
  'idSegmento',
  'idTemplate',
  'indStatus',
);

const mockResolvedValue = {
  data: {
    data: [
      {
        id: 1,
        nomSegmento: 'test',
      },
    ],
  },
};

const factory = () => createWrapper(
  LojaInputsGeralData,
  {
    $swal: jest.fn(),
  },
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
    data: () => ({
      segmentoRequest: {
        get: jest.fn(),
      },
    }),
  },
);

describe('LojaInputsGeralData', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .find(InputSelect, 1)
      .find(InputText, 3, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate input values', () => {
    const wrapper = factory();

    wrapper.vm.payload = mocksInputsValues;

    multipleInput(wrapper, inputs);
  });

  it('should be able a called segments request', async () => {
    const wrapper = factory();

    const spySegmentRequest = jest.spyOn(wrapper.vm.segmentoRequest, 'get')
      .mockResolvedValue(mockResolvedValue);

    wrapper.vm.segmentoRequest.get();

    expect(spySegmentRequest).toBeCalledTimes(1);
    await expect(wrapper.vm.segmentoRequest.get())
      .resolves.toEqual(mockResolvedValue);
  });
});
