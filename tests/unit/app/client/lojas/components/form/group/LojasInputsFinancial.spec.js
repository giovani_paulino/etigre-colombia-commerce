import testWrapper, { createEqualPayloadInputs, createWrapper } from '@test/helpers/testWrapper';
import LojaInputsFinancial from 'App/client/lojas/components/form/group/LojaInputsFinancial.vue';
import InputRadio from 'App/client/components/form/inputs/input/InputRadio.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import InputSelect from 'Arch/components/global-components/InputSelect.vue';
import { multipleInput } from '@test/helpers/inputTestHelper';

const mockInputsValues = {
  dadosFinanceiros: {
    idConfigBoleto: 1,
    emails: ['test@example.com'],
    indAtivadoVlrMaximoBoleto: true,
    indFaturaVendaFisica: true,
    indFaturaVendaOnline: true,
    indImprimeSegundaViaVenda: true,
    periodoGeracaoFaturaType: 'D',
    qtdDiasCorte: 2,
    sglMeioPagamento: 'B',
    vlrLimiteCorte: 500,
    vlrLimiteCredito: 100,
    vlrMaximoBoleto: 200,
  },
};

const mockPropsData = {
  payload: mockInputsValues,
};

const factory = () => createWrapper(
  LojaInputsFinancial,
  undefined,
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
  },
);

const inputs = createEqualPayloadInputs(
  mockInputsValues.dadosFinanceiros,
  'idConfigBoleto',
  'emails',
);

describe('LojaInputsFinancial', () => {
  afterEach(() => jest.clearAllMocks());

  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .find(InputRadio, 4)
      .find(InputSelect, 2)
      .find(InputText, 4, (input) => {
        expect(input.text()).toBeTruthy();
      });
  });

  it('should be able a validate all inputs', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    multipleInput(wrapper, inputs);
  });
});
