import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import EditLojaContent from 'App/client/lojas/components/form/EditLojaContent.vue';
import FormGroup from '@/app/arch/components/crud/components/FormGroup.vue';
import FormButtons from '@/app/client/components/form/buttons/FormButtons.vue';
import LojaInputsGeralData from 'App/client/lojas/components/form/group/LojaInputsGeralData.vue';
import LojaInputsFinancial from 'App/client/lojas/components/form/group/LojaInputsFinancial.vue';
import LojaInputsEndereco from 'App/client/lojas/components/form/group/LojaInputsEndereco.vue';
import Vue from 'vue';

const mockInputsValue = {
  dadosFinanceiros: {
    emails: [],
    idConfigBoleto: '',
    indAtivadoVlrMaximoBoleto: '',
    indFaturaVendaFisica: '',
    indFaturaVendaOnline: '',
    indImprimeSegundaViaVenda: '',
    periodoGeracaoFaturaType: '',
    qtdDiasCorte: '',
    sglMeioPagamento: '',
    updatedDate: '',
    vlrLimiteCorte: '',
    vlrLimiteCredito: '',
    vlrMaximoBoleto: '',
  },
  deletedDate: '',
  idCliente: '',
  idEndereco: '',
  idSegmento: '',
  idTemplate: '',
  indStatus: '',
  nomLoja: '',
  numCnpj: '',
  numEndereco: '',
  numTelefone: '',
  txtComplementoEndereco: '',
};

const mockPropsData = {
  payload: mockInputsValue,
};

const factory = () => createWrapper(
  EditLojaContent,
  undefined,
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
  },
);

describe('EditLojaContent', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render basic html components', () => {
    const wrapper = factory();
    testWrapper(wrapper)
      .find(FormGroup, 1)
      .find({ name: 'VTabs' }, 1);
  });

  it('should be able a render geral data', async () => {
    const wrapper = factory();

    wrapper.vm.tabs = 0;

    await Vue.nextTick();
    await Vue.nextTick();
    await Vue.nextTick();
    await Vue.nextTick();

    testWrapper(wrapper)
      .find(LojaInputsGeralData, 1)
      .find(FormButtons, 1);
  });

  it('should be able a render financial data', async () => {
    const wrapper = factory();

    wrapper.vm.tabs = 1;

    await Vue.nextTick();
    await Vue.nextTick();
    await Vue.nextTick();
    await Vue.nextTick();

    testWrapper(wrapper)
      .find(LojaInputsFinancial, 1)
      .find(FormButtons, 1);
  });

  it('should be able a render endereco data', async () => {
    const wrapper = factory();

    wrapper.vm.tabs = 2;

    await Vue.nextTick();
    await Vue.nextTick();
    await Vue.nextTick();
    await Vue.nextTick();

    testWrapper(wrapper)
      .find(LojaInputsEndereco, 1)
      .find(FormButtons, 1);
  });
});
