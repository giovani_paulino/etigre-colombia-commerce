import CreateLojaContent from 'App/client/lojas/components/form/CreateLojaContent.vue';
import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import FormGroup from '@/app/arch/components/crud/components/FormGroup.vue';
import FormButtons from '@/app/client/components/form/buttons/FormButtons.vue';
import LojaInputsGeralData from 'App/client/lojas/components/form/group/LojaInputsGeralData.vue';
import LojaInputsFinancial from 'App/client/lojas/components/form/group/LojaInputsFinancial.vue';
import LojaInputsEndereco from 'App/client/lojas/components/form/group/LojaInputsEndereco.vue';

const mockInputsValues = {
  dadosFinanceiros: {
    emails: [],
    idConfigBoleto: '',
    indAtivadoVlrMaximoBoleto: '',
    indFaturaVendaFisica: '',
    indFaturaVendaOnline: '',
    indImprimeSegundaViaVenda: '',
    periodoGeracaoFaturaType: '',
    qtdDiasCorte: '',
    sglMeioPagamento: '',
    updatedDate: '',
    vlrLimiteCorte: '',
    vlrLimiteCredito: '',
    vlrMaximoBoleto: '',
  },
  deletedDate: '',
  idCliente: '',
  idEndereco: '',
  idSegmento: '',
  idTemplate: '',
  indStatus: '',
  nomLoja: '',
  numCnpj: '',
  numEndereco: '',
  numTelefone: '',
  txtComplementoEndereco: '',
};

const mockPropsData = {
  payload: mockInputsValues,
};

const factory = () => createWrapper(
  CreateLojaContent,
  undefined,
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
  },
);

describe('CreateLojaContent', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render html components', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValues;

    testWrapper(wrapper)
      .find({ name: 'VStepper' }, 1)
      .find(FormGroup, 4)
      .find(FormButtons, 3)
      .find(LojaInputsEndereco, 1)
      .find(LojaInputsFinancial, 1)
      .find(LojaInputsGeralData, 1);
  });
});
