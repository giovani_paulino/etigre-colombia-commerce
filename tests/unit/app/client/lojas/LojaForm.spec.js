import testWrapper, { createWrapper } from '@test/helpers/testWrapper';
import LojaForm from 'App/client/lojas/LojaForm.vue';
import CrudForm from 'Arch/components/crud/CrudForm.vue';
import CreateLojaContent from 'App/client/lojas/components/form/CreateLojaContent.vue';
import EditLojaContent from 'App/client/lojas/components/form/EditLojaContent.vue';

const mockInputsValue = {
  dadosFinanceiros: {
    emails: ['test@example.com'],
    idConfigBoleto: 1,
    indAtivadoVlrMaximoBoleto: true,
    indFaturaVendaFisica: true,
    indFaturaVendaOnline: true,
    indImprimeSegundaViaVenda: true,
    periodoGeracaoFaturaType: 'D',
    qtdDiasCorte: 2,
    sglMeioPagamento: 'B',
    vlrLimiteCorte: 100,
    vlrLimiteCredito: 200,
    vlrMaximoBoleto: 300,
  },
  idCliente: 1,
  idEndereco: 2,
  idSegmento: 3,
  idTemplate: 4,
  indStatus: 'A',
  nomLoja: 'Dev store',
  numCnpj: '12345678978945',
  numEndereco: 12345,
  numTelefone: 5511912345678,
  txtComplementoEndereco: 'adifuaoidfuuya',
};

const mockPropsData = {
  backForm: jest.fn(),
  lojaId: 1,
  lockGetIdParams: true,
};

const mockResolvedValue = {
  data: mockInputsValue,
};

const factory = (mocks) => createWrapper(
  LojaForm,
  mocks,
  undefined,
  {
    propsData: {
      ...mockPropsData,
    },
  },
);

describe('LojaForm', () => {
  it('should be able a vue instance', () => {
    const wrapper = factory();

    testWrapper(wrapper);
  });

  it('should be able a content basic props', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .toContain(mockPropsData);
  });

  it('should be able a render basic html components', () => {
    const wrapper = factory();

    testWrapper(wrapper)
      .find(CrudForm, 1);
  });

  it('should be able a render loja edit content', () => {
    const wrapper = factory({
      $route: {
        params: {
          id: 1,
        },
      },
    });

    wrapper.vm.payload = mockInputsValue;
    wrapper.vm.lojaId = 1;
    wrapper.vm.lockGetIdParams = true;

    const editContent = wrapper.find(EditLojaContent);

    expect(editContent).toBeTruthy();
  });

  it('should be able a render loja create content', () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValue;
    wrapper.vm.lojaId = null;

    testWrapper(wrapper)
      .find(CreateLojaContent, 1);
  });

  it('should be able a create new loja', async () => {
    const wrapper = factory();

    wrapper.vm.payload = mockInputsValue;
    wrapper.vm.lojaId = false;

    const spyRequest = jest.spyOn(wrapper.vm.requestClass, 'save')
      .mockResolvedValue(mockResolvedValue);

    const form = wrapper.find(CrudForm);

    testWrapper(form);

    form.vm.create();

    expect(spyRequest).toBeCalledTimes(1);
    await expect(wrapper.vm.requestClass.save())
      .resolves.toEqual(mockResolvedValue);
  });

  it('should be able a edit loja', async () => {
    const wrapper = factory();

    wrapper.vm.lojaId = 1;
    wrapper.vm.payload = mockInputsValue;

    const spyRequest = jest.spyOn(wrapper.vm.requestClass, 'edit')
      .mockResolvedValue(mockResolvedValue);

    const form = wrapper.find(CrudForm);

    testWrapper(form);

    form.vm.update();

    expect(spyRequest).toBeCalledTimes(1);
    await expect(wrapper.vm.requestClass.edit())
      .resolves.toEqual(mockResolvedValue);
  });
});
