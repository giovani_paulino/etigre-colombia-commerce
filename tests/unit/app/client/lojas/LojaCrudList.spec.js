import LojaRequest from '@/app/client/lojas/LojaRequest';
import testCrudList from '@test/helpers/testCrudList';
import testWrapper from '@test/helpers/testWrapper';
import LojaCrudList from 'App/client/lojas/LojaCrudList.vue';
import DataTableDateColumn from 'Arch/components/data-table/DataTableColumns/DataTableDateColumn.vue';
import DataTableTextColumn from 'Arch/components/data-table/DataTableColumns/DataTableTextColumn.vue';

const listResolvedDataValueMock = [
  {
    nomLoja: 'Dev store',
    numTelefone: '5511912345678',
    updatedDate: new Date(),
  },
];

jest.mock('@/app/client/lojas/LojaRequest', () => ({
  list: jest.fn(),
  remove: jest.fn(),
}));

describe('LojaCrudList', () => {
  it('should be able a test all features', async () => {
    const wrapper = await testCrudList({
      componentCrudList: LojaCrudList,
      listResolvedDataValueMock,
      requestClassMock: LojaRequest,
      hideEditTest: true,
      mocks: {
        $route: {
          params: {
            id: 1,
          },
          query: {
            page: 1,
            limit: 15,
          },
        },
      },
      mockQuery: [1],
    });

    testWrapper(wrapper)
      .find(DataTableDateColumn, 1)
      .find(DataTableTextColumn, 2, (column) => {
        expect(column.text()).toBeTruthy();
      });
  });
});
