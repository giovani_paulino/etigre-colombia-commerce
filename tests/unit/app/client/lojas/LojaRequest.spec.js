import testCrudRequest from '@test/helpers/testCrudRequest';
import LojaRequest from 'App/client/lojas/LojaRequest';

describe('LojaRequest', () => {
  it('should a return on call with all methods', async () => {
    testCrudRequest(LojaRequest);
  });
});
