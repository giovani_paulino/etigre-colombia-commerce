import '@test/base-test';
import { mount } from '@vue/test-utils';
import ExampleFilter from 'Modules/modulo-um/submodulo/feature-submodulo/ExampleFilter.vue';

function factory(mock = {}) {
  return mount(ExampleFilter, {
    mocks: {
      $route: {
        query: {
          page: 1,
        },
      },
      ...mock,
    },
  });
}

describe('ExampleFilter.vue', () => {
  it('verifying if the component is an Vue Instance', () => {
    const wrapper = factory();
    expect(wrapper.exists()).toBeTruthy();
    wrapper.destroy();
  });
  it('verifying if the components in html are rendered', () => {
    const wrapper = factory();
    expect(wrapper.findAllComponents({ name: 'FilterTemplate' })).toHaveLength(1);
    wrapper.destroy();
  });
  it('Should push to query on filter', () => {
    const mockFn = jest.fn().mockResolvedValue();
    const wrapper = factory({
      $router: {
        push: mockFn,
      },
    });
    const filterTemplate = wrapper.findComponent({ name: 'FilterTemplate' });
    const spy = jest.spyOn(wrapper.vm, 'filter');

    filterTemplate.vm.$emit('submit');

    expect(spy).toBeCalledTimes(1);
    expect(mockFn).toBeCalledTimes(1);

    wrapper.destroy();
  });
});
