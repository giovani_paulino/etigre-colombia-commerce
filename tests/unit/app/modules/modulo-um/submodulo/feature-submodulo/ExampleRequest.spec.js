import ExampleRequest from 'Modules/modulo-um/submodulo/feature-submodulo/ExampleRequest';
import testCrudRequest from '@test/helpers/testCrudRequest';

describe('ExampleRequest.js', () => {
  it('Should return a promisse on call integration methods', async () => {
    testCrudRequest(ExampleRequest);
  });
});
