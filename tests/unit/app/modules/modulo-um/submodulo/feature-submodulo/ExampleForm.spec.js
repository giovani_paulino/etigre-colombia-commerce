import '@test/base-test';
import {
  createInput,
  findTextFieldInputById,
  expectFieldHasRequiredError,
} from '@test/helpers/inputTestHelper';
import { testErrorSubmit, testSuccessSubmit } from '@test/helpers/testCrudForm';
import { mount } from '@vue/test-utils';
import FormGroup from 'Arch/components/crud/components/FormGroup.vue';
import CrudForm from 'Arch/components/crud/CrudForm.vue';
import ExampleForm from 'Modules/modulo-um/submodulo/feature-submodulo/ExampleForm.vue';
import ExampleRequest from 'Modules/modulo-um/submodulo/feature-submodulo/ExampleRequest';

jest.mock('Modules/modulo-um/submodulo/feature-submodulo/ExampleRequest', () => ({
  save: jest.fn().mockResolvedValue({ data: {} }),
  get: jest.fn().mockResolvedValue({ data: { name: 'Nome de exemplo', calories: '55' } }),
  edit: jest.fn().mockResolvedValue({ data: { } }),
}));

function clearMocks() {
  ExampleRequest.save.mockClear();
  ExampleRequest.get.mockClear();
  ExampleRequest.edit.mockClear();
}

function factory(mocks) {
  const div = document.createElement('div');
  document.body.appendChild(div);
  return mount(ExampleForm, {
    attachTo: div,
    sync: false,
    mocks: {
      $route: { params: {} },
      ...mocks,
    },
  });
}

function createInputs(name = null, calories = null) {
  return [
    createInput('Nome', name),
    createInput('Calorias', calories),
  ];
}

function clearOverwriteMethods(wrapper) {
  const copy = wrapper;
  copy.vm.overrideCreatePayload = null;
  copy.vm.overrideSetEntityData = null;
  copy.vm.overrideEditPayload = null;
}

describe('ExampleForm.vue', () => {
  afterEach(() => {
    clearMocks();
  });

  it('verifying if the component is an Vue Instance', () => {
    const wrapper = factory();
    expect(wrapper).toBeTruthy();
    wrapper.destroy();
  });
  it('verifying if the components in html are rendered', () => {
    const wrapper = factory();
    expect(wrapper.findAllComponents(CrudForm)).toHaveLength(1);
    expect(wrapper.findAllComponents(FormGroup)).toHaveLength(2);
    wrapper.destroy();
  });

  it('should create if has no errors', async () => {
    const wrapper = factory();

    const name = 'Nome de exemplo';
    const calories = 55;
    const inputs = createInputs(name, calories);

    const assertPayload = { name: 'Nome de exemplo', calories: '55' };

    await testSuccessSubmit(wrapper, {
      requestClass: ExampleRequest,
      inputValues: inputs,
      assertionValue: assertPayload,
    });
    wrapper.destroy();
  });

  it('should edit if has no errors', async () => {
    const wrapper = factory({
      $route: { params: { id: 1 } },
    });

    const name = 'Nome de exemplo';
    const calories = 55;
    const inputs = createInputs(name, calories);

    const assertPayload = { name: 'Nome de exemplo', calories: '55' };

    await testSuccessSubmit(wrapper, {
      requestClass: ExampleRequest,
      inputValues: inputs,
      assertionValue: assertPayload,
      testEdit: true,
      submitFunctionName: 'update',
    });
    wrapper.destroy();
  });

  it('should not create if has input errors', async () => {
    const wrapper = factory();

    const name = null;
    const calories = null;
    const inputs = createInputs(name, calories);

    await testErrorSubmit(wrapper, {
      requestClass: ExampleRequest,
      inputValues: inputs,
    });

    const fieldNome = findTextFieldInputById(wrapper, 'Nome');
    const fieldCalorias = findTextFieldInputById(wrapper, 'Calorias');
    expectFieldHasRequiredError(fieldNome);
    expectFieldHasRequiredError(fieldCalorias);
    wrapper.destroy();
  });
});

describe('ExampleForm.vue with no overwrite methods', () => {
  afterEach(() => {
    clearMocks();
  });

  it('verifying if the component is an Vue Instance', () => {
    const wrapper = factory();
    expect(wrapper).toBeTruthy();
  });
  it('verifying if the components in html are rendered', () => {
    const wrapper = factory();
    expect(wrapper.findAllComponents(CrudForm)).toHaveLength(1);
    expect(wrapper.findAllComponents(FormGroup)).toHaveLength(2);
    wrapper.destroy();
  });

  it('should create if has no errors', async () => {
    const wrapper = factory();
    clearOverwriteMethods(wrapper);

    const name = 'Nome de exemplo';
    const calories = 55;
    const inputs = createInputs(name, calories);

    const assertPayload = { name: 'Nome de exemplo', calories: '55' };

    await testSuccessSubmit(wrapper, {
      requestClass: ExampleRequest,
      inputValues: inputs,
      assertionValue: assertPayload,
    });
    wrapper.destroy();
  });

  it('should edit if has no errors', async () => {
    const wrapper = factory({
      $route: { params: { id: 1 } },
    });
    clearOverwriteMethods(wrapper);

    const name = 'Nome de exemplo';
    const calories = 55;
    const inputs = createInputs(name, calories);

    const assertPayload = { name: 'Nome de exemplo', calories: '55' };

    await testSuccessSubmit(wrapper, {
      requestClass: ExampleRequest,
      inputValues: inputs,
      assertionValue: assertPayload,
      testEdit: true,
      submitFunctionName: 'update',
    });

    const form = wrapper.findComponent({ name: 'CrudForm' });
    const spy = jest.spyOn(form.vm, 'setEntityData');
    form.vm.loadEntityForEdit();
    await wrapper.vm.$nextTick();

    expect(spy).toBeCalledTimes(1);

    wrapper.destroy();
  });

  it('should not create if has input errors', async () => {
    const wrapper = factory();
    clearOverwriteMethods(wrapper);

    const name = null;
    const calories = null;
    const inputs = createInputs(name, calories);

    await testErrorSubmit(wrapper, {
      requestClass: ExampleRequest,
      inputValues: inputs,
    });

    const fieldNome = findTextFieldInputById(wrapper, 'Nome');
    const fieldCalorias = findTextFieldInputById(wrapper, 'Calorias');
    expectFieldHasRequiredError(fieldNome);
    expectFieldHasRequiredError(fieldCalorias);
    wrapper.destroy();
  });
});
