import testCrudList from '@test/helpers/testCrudList';
import DataTableTransformValueColumn from 'Arch/components/data-table/DataTableColumns/DataTableTransformValueColumn.vue';
import ExampleCrudList from 'Modules/modulo-um/submodulo/feature-submodulo/ExampleCrudList.vue';
import ExampleRequest from 'Modules/modulo-um/submodulo/feature-submodulo/ExampleRequest';

jest.mock('Modules/modulo-um/submodulo/feature-submodulo/ExampleRequest', () => ({
  list: jest.fn(),
  remove: jest.fn(),
}));

describe('ExampleCrudList.vue', () => {
  const mockListResolvedDataValue = [
    {
      id: 1,
      name: 'Frozen Yogurt',
      calories: 159,
      company: 'danone',
    },
    {
      id: 2,
      name: 'Yogurt',
      calories: 159,
      company: 'danone',
    },
  ];
  it('Should test all ExampleCrudList features', async () => {
    const wrapper = await testCrudList({
      componentCrudList: ExampleCrudList,
      requestClassMock: ExampleRequest,
      listResolvedDataValueMock: mockListResolvedDataValue,
    });
    const transformColumns = wrapper.findAllComponents(DataTableTransformValueColumn);
    expect(transformColumns).toHaveLength(2);
    transformColumns.wrappers.forEach((column) => {
      expect(column.text()).toBeTruthy();
    });

    // const textColumns = wrapper.findAllComponents(DataTableTextColumn);
    // expect(textColumns).toHaveLength(6);
    // textColumns.wrappers.forEach((column) => {
    //   expect(column.text()).toBeTruthy();
    // });
    // const cpfColumn = wrapper.findAllComponents(DataTableCpfCnpjColumn);
    // expect(cpfColumn).toHaveLength(2);
    // cpfColumn.wrappers.forEach((column) => {
    //   expect(column.text()).toBeTruthy();
    // });
  });
});
