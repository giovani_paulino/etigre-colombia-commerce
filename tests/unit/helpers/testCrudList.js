import { mount, createLocalVue, config } from '@vue/test-utils';
import Vue from 'vue';
import flushPromises from 'flush-promises';
import Vuetify from 'vuetify';
import Vuex from 'vuex';
import store from 'Arch/store';

config.mocks.$route = {
  query: {},
};

const localVue = createLocalVue();

localVue.use(Vuex);

function mockRequestClass(mockListReject, requestClassMock, listResolvedDataValueMock) {
  if (mockListReject) {
    requestClassMock.list.mockClear();
    requestClassMock.list.mockRejectedValue();
  } else {
    requestClassMock.list.mockClear();
    requestClassMock.list.mockResolvedValue({
      data: {
        content: listResolvedDataValueMock,
        currentPage: 1,
        limit: 15,
        totalPages: 1,
        totalElements: 2,
      },
    });
  }
}

async function crudListFactory(
  {
    componentCrudList,
    requestClassMock,
    listResolvedDataValueMock,
    stubs = {},
    mocks = {},
    mockMethods = {},
    mockListReject = false,
    mockData = {},
  },
) {
  mockRequestClass(mockListReject, requestClassMock, listResolvedDataValueMock);
  const vuetify = new Vuetify();
  const wrapper = mount(componentCrudList, {
    localVue,
    vuetify,
    store,
    data() {
      return {
        ...mockData,
      };
    },
    stubs: {
      ...stubs,
    },
    mocks: {
      ...mocks,
    },

  });
  if (mockMethods) {
    Object.keys(mockMethods).forEach((mockName) => {
      wrapper.vm[mockName] = mockMethods[mockName];
    });
  }
  expect(requestClassMock.list).toBeCalledTimes(1);
  await flushPromises();
  return wrapper;
}

async function openActionMenu(wrapper) {
  const menu = wrapper.find('.actionMenuButton');
  menu.trigger('click');
  await Vue.nextTick();
}

async function testEditClick(vm, wrapper) {
  const wrapperCopy = wrapper;
  await Vue.nextTick();
  expect(vm).toHaveProperty('editRoute');
  const navigateMock = jest.fn().mockResolvedValue();
  wrapperCopy.vm.navigateTo = navigateMock;

  await openActionMenu(wrapperCopy);

  const editButton = wrapperCopy.find('.dataTableEditButton');
  editButton.trigger('click');
  expect(navigateMock).toBeCalledTimes(1);
}

async function testDeleteSuccess(wrapper, requestClassMock) {
  const wrapperCopy = wrapper;
  const confirmationDialogMock = jest.fn().mockResolvedValue({ value: true });
  const successDialogMock = jest.fn().mockResolvedValue({ value: true });
  const spyGetData = jest.spyOn(wrapper.vm, 'getData');
  const spyRemoveFromList = jest.spyOn(wrapperCopy.vm, 'removeFromListAfterDelete');
  await openActionMenu(wrapperCopy);

  const deleteButton = wrapperCopy.find('.dataTableDeleteButton');
  confirmationDialogMock.mockClear();
  successDialogMock.mockClear();
  spyRemoveFromList.mockClear();
  spyGetData.mockClear();

  requestClassMock.remove.mockClear();
  requestClassMock.remove.mockResolvedValue();

  wrapperCopy.vm.showConfirmDeleteDialog = confirmationDialogMock;
  wrapperCopy.vm.showSuccessToast = successDialogMock;
  deleteButton.trigger('click');
  await flushPromises();
  expect(confirmationDialogMock).toBeCalledTimes(1);
  expect(requestClassMock.remove).toBeCalledTimes(1);
  expect(spyRemoveFromList).toBeCalledTimes(1);
  expect(spyGetData).toBeCalledTimes(1);
}

async function testDeleteFail(wrapper, requestClassMock) {
  const wrapperCopy = wrapper;
  const errorToastMock = jest.fn().mockResolvedValue({ value: true });
  const spyGetData = jest.spyOn(wrapper.vm, 'getData');
  const spyRemoveFromList = jest.spyOn(wrapperCopy.vm, 'removeFromListAfterDelete');
  await openActionMenu(wrapperCopy);

  const deleteButton = wrapperCopy.find('.dataTableDeleteButton');
  spyRemoveFromList.mockClear();
  spyGetData.mockClear();
  errorToastMock.mockClear();

  requestClassMock.remove.mockClear();
  requestClassMock.remove.mockRejectedValue({
    response: {
      data: {
        message: 'Error',
      },
    },
  });

  wrapperCopy.vm.showErrorToast = errorToastMock;
  deleteButton.trigger('click');
  await flushPromises();
  expect(errorToastMock).toBeCalledTimes(1);
  expect(requestClassMock.remove).toBeCalledTimes(1);
  expect(spyRemoveFromList).not.toBeCalled();
  expect(spyGetData).toBeCalledTimes(1);
}

export default async function testCrudList({
  componentCrudList,
  requestClassMock,
  listResolvedDataValueMock,
  stubs = {},
  runExpectForRequests = true,
  runExpectForDelete = true,
  mocks = {},
  hideEditTest = false,
  mockQuery = [],
}) {
  const wrapper = await crudListFactory(
    {
      componentCrudList,
      requestClassMock,
      listResolvedDataValueMock,
      stubs,
      mocks,
    },
  );

  const { vm } = wrapper;
  expect(vm).toHaveProperty('headers');
  expect(vm.headers.length).toBeGreaterThan(0);
  expect(vm).toHaveProperty('requestClass');

  if (runExpectForRequests) {
    expect(requestClassMock.list).toBeCalledTimes(1);
    if (!mockQuery) {
      expect(requestClassMock.list).toBeCalledWith({ limit: 15, page: 1 });
    } else {
      expect(requestClassMock.list).toBeCalledWith({ limit: 15, page: 1 }, ...mockQuery);
    }
  }

  await flushPromises();
  const tBody = wrapper.find('tbody');
  expect(tBody.text()).not.toBe('support.components.dataTable.noDataText');
  expect(tBody.findAll('tr')).toHaveLength(listResolvedDataValueMock.length);

  if (!vm.hideEdit) {
    if (!hideEditTest) {
      await testEditClick(vm, wrapper);
    }
  }

  if (!vm.hideDelete && runExpectForDelete) {
    await testDeleteSuccess(wrapper, requestClassMock);
    await testDeleteFail(wrapper, requestClassMock);
  }

  await flushPromises();
  return wrapper;
}

function expectCrudListHasColumnsOf(wrapper, columnComponnent, total) {
  const transformColumns = wrapper.findAllComponents(columnComponnent);
  expect(transformColumns).toHaveLength(total);
  transformColumns.wrappers.forEach((column) => {
    expect(column.text()).toBeTruthy();
  });
}

export {
  crudListFactory,
  expectCrudListHasColumnsOf,
  testDeleteSuccess,
};
