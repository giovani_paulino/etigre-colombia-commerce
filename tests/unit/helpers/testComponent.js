const findAllComponents = (findComponent, wrapper) => wrapper.findAllComponents(findComponent);

const testComponent = (wrapper, wrapperMsg = '') => {
  expect(wrapper, wrapperMsg).toBeTruthy();

  const destroy = () => wrapper.destroy();

  const find = (component, length) => {
    const components = findAllComponents(component, wrapper);

    expect(components).toHaveLength(length);
    Array(length).forEach((_, i) => expect(components.at(i).exists()).toBeTruthy());

    return { ...testComponent(wrapper, wrapperMsg) };
  };

  const contain = (component, contained) => {
    const contentWrapper = findAllComponents(component, wrapper);
    expect(contentWrapper.at(0).text()).toContain(contained);

    return { ...testComponent(wrapper, wrapperMsg) };
  };

  return { find, destroy, contain };
};

export default testComponent;
