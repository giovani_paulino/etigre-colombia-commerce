import CrudForm from 'Arch/components/crud/CrudForm.vue';
import Vue from 'vue';
import flushpromise from 'flush-promises';
import { handleSetInputsValues, multipleInput } from './inputTestHelper';

const VueTestUtils = require('@vue/test-utils');

const swalMock = jest.fn();
VueTestUtils.config.mocks.$swal = swalMock;

const routerPushMock = jest.fn();
VueTestUtils.config.mocks.$router = {
  push: routerPushMock,
};

export async function testSuccessSubmit(wrapper, {
  requestClass,
  inputValues,
  assertionValue = {},
  submitFunctionName = 'create',
  formType = CrudForm,
  requestMethod = 'save',
  testEdit = false,
  notTestinput = false,
}) {
  if (requestClass === undefined) {
    throw new Error('Deve ser fornecido uma classe de request');
  }

  if (inputValues === undefined) {
    throw new Error('Deve ser fornecido um array de inputs no formato: {key: "", value: any}');
  }

  swalMock.mockClear();
  routerPushMock.mockClear();
  const reportForm = wrapper.findComponent(formType);
  const spyValidate = jest.spyOn(reportForm.vm, 'validateForm');
  const spyGenerate = jest.spyOn(reportForm.vm, submitFunctionName);

  if (!notTestinput) {
    multipleInput(wrapper, inputValues);
  } else {
    inputValues.forEach((element) => {
      const component = wrapper.find(element.key);
      handleSetInputsValues(component, element);
    });
  }

  await Vue.nextTick();

  await flushpromise();
  const submitButton = wrapper.find('[type="submit"]');
  await Vue.nextTick();
  submitButton.trigger('click');
  await Vue.nextTick();

  const form = wrapper.findComponent({ name: 'v-form' });
  if (!form.vm.value) {
    const errors = form.vm.inputs.map((item) => item.errorBucket);
    console.error('Erros do formulário:', errors);
  }

  await Vue.nextTick();
  expect(spyValidate).toHaveBeenCalled();
  expect(spyValidate, 'Formulário deveria estar valido').toHaveReturnedWith(true);
  expect(spyGenerate).toHaveBeenCalled();
  if (testEdit) {
    expect(requestClass.edit || requestClass[requestMethod]).toBeCalledTimes(1);
    expect(requestClass.edit || requestClass[requestMethod]).toBeCalledWith(1, assertionValue);
  } else {
    expect(requestClass.save || requestClass[requestMethod]).toBeCalledTimes(1);
    expect(requestClass.save || requestClass[requestMethod]).toBeCalledWith(assertionValue);
  }
  expect(swalMock).toHaveBeenCalledTimes(1);
  expect(routerPushMock).toBeCalledTimes(1);
}

export async function testErrorSubmit(wrapper, {
  requestClass,
  inputValues,
  submitFunctionName = 'create',
  formType = CrudForm,
}) {
  if (requestClass === undefined) {
    throw new Error('Deve ser fornecido uma classe de request');
  }

  if (inputValues === undefined) {
    throw new Error('Deve ser fornecido um array de inputs no formato: {key: "", value: any}');
  }

  swalMock.mockClear();
  routerPushMock.mockClear();
  const reportForm = wrapper.findComponent(formType);
  const spyValidate = jest.spyOn(reportForm.vm, 'validateForm');
  const spyGenerate = jest.spyOn(reportForm.vm, submitFunctionName);
  multipleInput(wrapper, inputValues);

  const submitButton = wrapper.find('[type="submit"]');
  submitButton.trigger('click');
  Vue.nextTick(() => {
    expect(spyValidate).toHaveBeenCalled();
    expect(spyValidate).toHaveReturnedWith(false);
    expect(spyGenerate).not.toBeCalled();
    expect(requestClass.save || requestClass.generate).not.toBeCalled();
    Vue.nextTick(() => {
      expect(swalMock).toHaveBeenCalledTimes(1);
      expect(routerPushMock).not.toBeCalled();
    });
  });
}
