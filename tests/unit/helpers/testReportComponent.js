import ButtonPrimary from 'Arch/components/Buttons/ButtonPrimary.vue';
import testWrapper from './testWrapper';

const testVueInstance = async (wrapper, crudListName) => {
  await wrapper.vm.$root.$emit(`total-itens-${crudListName}`, 10);

  testWrapper(wrapper);
  expect(wrapper.vm.totalItens).toBe(10);
};

const testRenderComponents = (wrapper) => {
  testWrapper(wrapper)
    .find(ButtonPrimary, 1, (button) => expect(button.text()).toBeTruthy());
};

const testOpenDialog = async (wrapper, swallMock, mockRequestClass) => {
  swallMock.mockResolvedValue({ value: true });

  mockRequestClass.terminateCsv.mockResolvedValue({});

  await wrapper.vm.$nextTick();

  const submitButton = wrapper.find('[type="submit"]');

  submitButton.trigger('click');

  wrapper.vm.confirmTerminate();

  expect(swallMock).toBeCalled();

  testWrapper(wrapper);
};

const testDontReport = async (wrapper, swallMock, mockRequestClass) => {
  swallMock.mockResolvedValue({ value: false });

  mockRequestClass.terminateCsv.mockResolvedValue({ });

  const spyTerminateAccount = jest.spyOn(wrapper.vm, 'terminateCallback');

  await wrapper.vm.$nextTick();

  const submitButton = wrapper.find('[type="submit"]');

  submitButton.trigger('click');

  wrapper.vm.confirmTerminate();

  expect(spyTerminateAccount).toBeCalledTimes(0);

  testWrapper(wrapper);
};

export default async function testReportCompont({
  componentWrapper = {}, crudListName = '', mockRequestClass = {}, swallMock = {},
}) {
  await testVueInstance(componentWrapper, crudListName);

  testRenderComponents(componentWrapper);

  if (!mockRequestClass || !swallMock) {
    throw new Error('Request class or swalmock is required');
  }

  await testOpenDialog(componentWrapper, swallMock, mockRequestClass);
  await testDontReport(componentWrapper, swallMock, mockRequestClass);
}
