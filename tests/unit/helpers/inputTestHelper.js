export function handleSetInputsValues(wrapper, element) {
  const component = wrapper;
  component.element[element.valueKey || 'value'] = element.value;
  if (element.shouldEmitEventInsteadOfTrigger) {
    component.vm.$emit(element.event, element.value);
  } else {
    component.trigger(element.event || 'input');
  }
}

export function singleInput(wrapper, element) {
  const component = wrapper.find(element.key);
  expect(component).toBeTruthy();
  expect(component.element, `Elemento: ${element.key} não encontrado`).toBeTruthy();

  handleSetInputsValues(component, element);
}

export function multipleInput(wrapper, elements) {
  elements.forEach((element) => {
    singleInput(wrapper, element);
  });
}

function buildInputIdAttr(label) {
  return `[id="${label}"]`;
}

function findInputById(wrapper, label, name) {
  const inputs = wrapper.findAllComponents({ name });
  expect(inputs.length).toBeGreaterThanOrEqual(1);
  let selectedInput = null;
  const inputId = buildInputIdAttr(`input:${label}`);

  inputs.wrappers.forEach((input) => {
    const search = input.find(inputId);
    if (search.exists()) {
      selectedInput = input;
    }
  });
  expect(selectedInput.exists()).toBe(true);

  return selectedInput;
}

export function findTextFieldInputById(wrapper, label) {
  const input = findInputById(wrapper, label, 'VTextField');
  return input;
}

export function findSelectInputById(wrapper, label) {
  const input = findInputById(wrapper, label, 'VSelect');
  return input;
}

export function expectFieldHasError(field, error) {
  expect(field.vm.errorBucket, `Campo ${field.vm.label} deveria apresentar o erro: ${error}`).toContain(error);
}

export function expectFieldHasRequiredError(field) {
  expectFieldHasError(field, 'rules.requiredErrorMessage');
}

export function expectFieldHasConfirmationError(field) {
  expectFieldHasError(field, 'support.components.inputs.confirmationError');
}

export function expectFieldHasIdenticalError(field) {
  expectFieldHasError(field, 'support.components.inputs.identical');
}

export function expectFieldHasOnlyAlphabeticCharactersError(field) {
  expectFieldHasError(field, 'rules.onlyAlphabeticCharactersErrorMessage');
}

export function expectFieldHasMaxLengthError(field) {
  expectFieldHasError(field, 'rules.maxLengthErrorMessage');
}

export function expectFieldHasMinLengthError(field) {
  expectFieldHasError(field, 'rules.minLengthErrorMessage');
}

export function createInputById(id, value) {
  return {
    key: buildInputIdAttr(id),
    value,
  };
}

export function createInput(id, value) {
  return createInputById(`input:${id}`, value);
}

export function createInputCombobox(inputId, value) {
  return {
    key: `${inputId} > .v-select`,
    value,
    shouldEmitEventInsteadOfTrigger: true,
    event: 'input',
  };
}

export function createInputUpload(inputId, value) {
  return {
    key: inputId,
    value,
    shouldEmitEventInsteadOfTrigger: true,
    event: 'input',
  };
}

export function createInputRadio(inputId, value) {
  return {
    key: `${inputId} .v-input--radio-group .v-radio`,
    value,
    shouldEmitEventInsteadOfTrigger: true,
    event: 'change',
  };
}
