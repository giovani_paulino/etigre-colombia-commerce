import '@test/base-test';
import Vuetify from 'vuetify';
import { createInput } from '@test/helpers/inputTestHelper';
import { mount } from '@vue/test-utils';
import store from 'Arch/store';

const VueTestUtils = require('@vue/test-utils');

const swalMock = jest.fn();
VueTestUtils.config.mocks.$swal = swalMock;

export function createEqualPayloadInputs(payload, ...ignores) {
  const inputs = [];

  Object.keys(payload).forEach((key) => {
    if (!ignores.includes(key)) {
      inputs.push(createInput(key, payload[key]));
    }
  });

  return inputs;
}

export function createWrapper(Component, mocks = {}, stubs = [], ...rest) {
  const vuetify = new Vuetify();
  const div = document.createElement('div');
  document.body.appendChild(div);
  return mount(Component, {
    vuetify,
    attachTo: div,
    sync: false,
    store,
    mocks: {
      $route: { params: {} },
      ...mocks,
    },
    stubs: [...stubs],
    ...rest,
  });
}

const testExist = (w) => {
  expect(w).toBeTruthy();
  expect(w.vm).toBeTruthy();
  expect(w.isVueInstance()).toBeTruthy();
};

const findAll = (wrapper, component) => wrapper.findAll(component);

export default function testWrapper(wrapper) {
  swalMock.mockClear();
  testExist(wrapper);

  const find = (component, length, cb = null) => {
    const items = findAll(wrapper, component);

    expect(items).toHaveLength(length);
    items.wrappers.forEach((item) => {
      testExist(item);
      if (cb) {
        cb(item);
      }
    });

    return testWrapper(wrapper);
  };

  const toContain = (props) => {
    Object.keys(props).forEach((key) => {
      expect(wrapper.vm).toHaveProperty(key);
    });

    return testWrapper(wrapper);
  };

  const contains = (props) => {
    Object.keys(props).forEach((key) => {
      expect(wrapper.find(props[key])).toBeTruthy();
    });

    return testWrapper(wrapper);
  };

  wrapper.destroy();

  return { find, toContain, contains };
}
