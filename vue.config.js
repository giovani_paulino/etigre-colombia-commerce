const path = require('path');

module.exports = {
  lintOnSave: 'error',
  transpileDependencies: ['vuetify'],
  devServer: {
    proxy: {
      '/api': {
        target: 'https://api.dev.k8s.cardhub.com.br/',
        changeOrigin: true,
        pathRewrite: {
          '^/api': '',
        },
      },
    },
    disableHostCheck: true,
  },
  configureWebpack: {
    resolve: {
      alias: {
        App: path.resolve(__dirname, 'src/app/'),
        Arch: path.resolve(__dirname, 'src/app/arch/'),
        Assets: path.resolve(__dirname, 'src/assets/'),
        Modules: path.resolve(__dirname, 'src/app/modules/'),
        Locales: path.resolve(__dirname, 'src/locales/'),
        Plugins: path.resolve(__dirname, 'src/plugins/'),
        '@test': path.resolve(__dirname, 'tests/unit/'),
      },
    },
  },

  pluginOptions: {
    i18n: {
      locale: 'pt',
      fallbackLocale: 'pt',
      localeDir: 'locales',
      enableInSFC: false,
    },
  },
};
