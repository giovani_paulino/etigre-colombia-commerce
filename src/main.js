import '@/plugins/config-env';
import Vue from 'vue';
import VueTheMask from 'vue-the-mask';
import store from 'Arch/store';
import App from './App.vue';
import './plugins/global-components';
import i18n from './plugins/i18n';
import './plugins/sweetalert2';
import vuetify from './plugins/vuetify';
import router from './router';
import './sass/colors.sass';
import './sass/font.sass';
import './sass/theme.sass';
import './sass/utils.sass';
import './plugins/loading';

Vue.config.productionTip = false;
Vue.use(VueTheMask);

new Vue({
  router,
  vuetify,
  i18n,
  render: (h) => h(App),
  store,
}).$mount('#app');
