import InputSelect from 'Arch/components/global-components/InputSelect.vue';
import InputText from 'Arch/components/global-components/InputText.vue';
import InputUpload from 'Arch/components/global-components/InputUpload.vue';
import { VueEditor } from 'vue2-editor';
import Vue from 'vue';

const GlobalComponents = {
  install: (VueInstance) => {
    VueInstance.component('input-text', InputText);
    VueInstance.component('input-select', InputSelect);
    VueInstance.component('input-upload', InputUpload);
    VueInstance.component('vue-editor', VueEditor);
  },
};

Vue.use(GlobalComponents);
