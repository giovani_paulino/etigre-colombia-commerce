import Vue from 'vue';
import VueSweetalert2 from 'vue-sweetalert2';
import 'sweetalert2/dist/sweetalert2.min.css';
import '@/sass/sweet-alert.sass';

Vue.use(VueSweetalert2);
