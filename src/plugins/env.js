const BASE_API_URL = '/'; // default
const APP_KEY = '-----BEGIN PUBLIC KEY-----\nMIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQDb+FK1NtPy8gsVcuygrSFMq+zU\nAHex90bJ1n3XkhtPCOXsOuBvrtcAVUbrm4XE6ahhiwiWueDv1e3C/gH57on7Vc0p\nHNs1p/H83EAS/xHUEmMaP2Yv8oDAzO5QPz37f3k9qhDFX3i8fSPKV/SjHHrHXlAh\nW8FAPa7UDTmw8HROwQIDAQAB\n-----END PUBLIC KEY-----';
module.exports = {
  BASE_API_URL,
  APP_KEY,
  install: (Vue, config) => {
    const vueInstance = Vue;
    vueInstance.env = {};
    vueInstance.env.APP_KEY = config.APP_KEY || APP_KEY;
    vueInstance.env.BASE_API_URL = config.BASE_API_URL || BASE_API_URL;

    vueInstance.prototype.$env = vueInstance.env;
  },
};
