import ParametrosRoutes from 'Modules/modulo-dois/feature-modulo-dois/routes';
import { buildModuleRoute } from 'Arch/helpers/RouterHelper';

export default [
  buildModuleRoute({
    path: '/modulo-dois',
    moduleId: 'modulo-dois',
    moduleName: 'Módulo Dois',
    moduleIcon: 'mdi-dns',
    children: [
      ...ParametrosRoutes,
    ],
  }),
];
