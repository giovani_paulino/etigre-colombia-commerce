export default [
  {
    text: 'Dashboard',
    disabled: false,
    to: 'dashboard',
  },
  {
    text: 'Módulo Dois',
    disabled: false,
    to: 'modulo-dois.submodules.list',
  },
];
