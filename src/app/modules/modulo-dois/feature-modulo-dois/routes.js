export default [
  {
    path: 'feature-modulo-dois/',
    name: 'modulo-dois.parametros.list',
    component: () => import('Arch/template/DefaultRouterView.vue'),
    meta: {
      moduleActive: 'modulo-dois',
      card: {
        title: 'Feature Módulo Dois',
        icon: 'home',
      },
    },
  },
];
