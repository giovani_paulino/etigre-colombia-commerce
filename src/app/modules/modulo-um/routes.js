import Submodule from 'Modules/modulo-um/submodulo/routes';
import FeatureUm from 'Modules/modulo-um/feature-modulo-um/routes';
import { buildModuleRoute } from 'Arch/helpers/RouterHelper';

export default [
  buildModuleRoute({
    path: '/modulo-um',
    moduleId: 'modulo-um',
    moduleName: 'Módulo Um',
    moduleIcon: 'mdi-view-dashboard',
    children: [
      ...FeatureUm,
      ...Submodule,
    ],
  }),
];
