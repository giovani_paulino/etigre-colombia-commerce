export default [
  {
    text: 'Dashboard',
    disabled: false,
    to: 'dashboard',
  },
  {
    text: 'Módulo Um',
    disabled: false,
    to: 'modulo-um.submodules.list',
  },
];
