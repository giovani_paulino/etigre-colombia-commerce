export default [
  {
    path: 'feature-modulo-um/',
    name: 'modulo-um.feature-modulo-um.list',
    component: () => import('Arch/template/DefaultRouterView.vue'),
    meta: {
      moduleActive: 'modulo-um',
      card: {
        title: 'Feature módulo um',
        icon: 'home',
      },
    },
  },
];
