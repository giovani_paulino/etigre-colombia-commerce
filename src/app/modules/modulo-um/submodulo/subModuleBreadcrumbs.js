import moduleBreadcrumbs from 'Modules/modulo-um/moduleBreadcrumbs';

export default [
  ...moduleBreadcrumbs,
  {
    text: 'SubmóduloUm',
    disabled: false,
    to: 'modulo-um.submodules.list',
  },
];
