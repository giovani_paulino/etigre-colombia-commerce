import CrudRequest from 'Arch/components/crud/MockCrudRequest';
import MockHelper from 'Arch/components/crud/MockHelper';

class ExampleRequest extends CrudRequest {
  static getMockList() {
    const base = {
      id: 1,
      name: 'Frozen Yogurt',
      calories: 159,
      company: 'danone',
    };
    return MockHelper.replicate(base, 20);
  }
}

export default ExampleRequest;
