import submoduleBreadcrumbs from 'Modules/modulo-um/submodulo/subModuleBreadcrumbs';
import { buildCrudRoutes } from 'Arch/components/crud/CrudRouter';

const featureBreadcrumb = [
  ...submoduleBreadcrumbs,
  {
    text: 'Feature submodulo',
    disabled: true,
  },
];

export default [
  ...buildCrudRoutes({
    basePath: 'feature-submodulo',
    baseName: 'modulo-um.submodulo.feature-submodulo',
    moduleActive: 'modulo-um',
    subModuleActive: 'submodulo',
    baseBreadcrumbs: featureBreadcrumb,
    card: {
      title: 'Feature submodulo',
      icon: 'home',
    },
    listRoute: {
      btnText: 'Novo Exemplo',
      filterComponent: () => import('Modules/modulo-um/submodulo/feature-submodulo/ExampleFilter.vue'),
      navigateBackRoute: 'modulo-um.submodules.list',
      newRouteName: 'modulo-um.submodulo.feature-submodulo.new',
      pageContent: () => import('Modules/modulo-um/submodulo/feature-submodulo/ExampleCrudList.vue'),
      pageTitle: 'Listagem Crud Exemplo',
      searchComponent: () => import('Arch/components/crud/components/OnlySearchFilter.vue'),
      searchOptions: { label: 'Pesquisar exemplo', searchAttribute: 'search' },
    },
    newRoute: {
      component: () => import('Modules/modulo-um/submodulo/feature-submodulo/ExampleForm.vue'),
      breadcrumbs: [
        {
          text: 'Adicionar exemplo',
          disabled: true,
        },
      ],
    },
    editRoute: {
      component: () => import('Modules/modulo-um/submodulo/feature-submodulo/ExampleForm.vue'),
      breadcrumbs: [
        {
          text: 'Editar exemplo',
          disabled: true,
        },
      ],
    },
  }),
];
