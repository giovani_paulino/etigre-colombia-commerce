import TemplateRoutes from 'Modules/modulo-um/submodulo/feature-submodulo/routes';
import { buildSubModuleRoute } from 'Arch/helpers/RouterHelper';

export default [
  buildSubModuleRoute({
    moduleId: 'modulo-um',
    name: 'submodulo',
    path: 'submodulo/',
    subModuleTitle: 'Submódulo',
    children: [
      ...TemplateRoutes,
    ],
  }),
];
