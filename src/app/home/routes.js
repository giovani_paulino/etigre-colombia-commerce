import { buildCrudRoutes } from '../arch/components/crud/CrudRouter';

const featureBreadcrumb = [
  {
    text: 'Home',
    disabled: false,
    to: 'home',
  },
];

export default [
  ...buildCrudRoutes({
    baseName: 'home',
    basePath: '/home',
    moduleActive: 'home',
    baseBreadcrumbs: featureBreadcrumb,
    listRoute: {
      navigateBackRoute: 'dashboard',
      pageTitle: 'Dados',
      pageContent: () => import('App/home/Home.vue'),
      filterComponent: () => import('@/app/client/components/table/filters/ClientFilter.vue'),
    },
  }),
];
