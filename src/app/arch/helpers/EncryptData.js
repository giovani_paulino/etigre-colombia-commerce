import NodeRSA from 'node-rsa';
import Env from '@/plugins/env';

export default function encryptData(data) {
  const publicKey = Env.APP_KEY.replace(/\\n/g, '\n');
  const key = new NodeRSA();
  key.importKey(publicKey);
  return key.encrypt(data, 'base64');
}
