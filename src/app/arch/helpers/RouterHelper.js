export function buildModuleRoute({
  path,
  moduleId,
  moduleName,
  moduleIcon,
  children,
}) {
  return {
    path,
    component: () => import('Arch/template/DefaultRouterView.vue'),
    meta: {
      module: {
        moduleId,
        moduleName,
        moduleIcon,
        subModuleListRouteName: `${moduleId}.submodules.list`,
      },
    },
    children: [
      {
        path: '',
        name: `${moduleId}.submodules.list`,
        component: () => import('Arch/modules-pages/SubModuleList.vue'),
        meta: {
          hideFromSidebar: true,
          moduleActive: moduleId,
          breadcrumbs: [
            {
              text: 'Dashboard',
              disabled: false,
              to: 'dashboard',
            },
            {
              text: moduleName,
              disabled: true,
            },
          ],
        },
      },
      ...children,
    ],
  };
}

export function buildSubModuleRoute({
  path,
  name,
  subModuleTitle,
  children,
  moduleId,
}) {
  return {
    path,
    component: () => import('Arch/template/DefaultRouterView.vue'),
    name,
    meta: {
      moduleId,
      isSubModule: true,
      subModuleTitle,
    },
    children,
  };
}
