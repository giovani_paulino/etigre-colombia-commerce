export default [
  {
    color: '#3FD65F',
    textColor: 'success',
    label: 'Aprovado',
  },
  {
    color: '#F15F5F',
    textColor: 'error',
    label: 'Recusado',
  },
  {
    color: '#F15F5F',
    textColor: 'error',
    label: 'Cancelado',
  },
  {
    color: '#E2B801',
    textColor: 'warning',
    label: 'Pendente - Conta digital',
  },
  {
    color: '#E2B801',
    textColor: 'warning',
    label: 'Pendente - Integradora',
  },
];
