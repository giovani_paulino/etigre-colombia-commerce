export default [
  { value: 'Simple', text: 'Simples' },
  { value: 'Discounted', text: 'Descontada' },
  { value: 'Linked', text: 'Vinculada' },
  { value: 'Guaranteed', text: 'Caucacionada' },
  { value: 'InsurancePremium', text: 'Prêmio de seguro' },
  { value: 'Vendor', text: 'Vendor' },
];
