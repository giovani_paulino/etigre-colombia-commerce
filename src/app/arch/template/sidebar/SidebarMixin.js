export default {
  data: () => ({
    activeClass: {
      menuActive: true,
      bold: true,
      primary500: true,
      backgroundPrimary500: true,
    },
  }),
};
