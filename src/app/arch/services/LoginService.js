import {
  saveToken,
  clearLocalStorage,
} from 'Arch/services/LocalStorageService';
import http from 'Arch/services/HttpService';

function login(user) {
  return new Promise((resolve, reject) => http
    .post('api/account/auth/cardbackoffice/login', user).then(
      ({ data }) => {
        saveToken(data);
        resolve(data);
      },
      (error) => {
        reject(error);
      },
    ));
}

function logout() {
  return new Promise((resolve, reject) => http
    .post('/api/account/auth/logout').then(
      ({ data }) => {
        clearLocalStorage();
        resolve(data);
      },
      (error) => {
        clearLocalStorage();
        reject(error);
      },
    ));
}

export {
  login,
  logout,
};
