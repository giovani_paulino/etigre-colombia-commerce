import http from './HttpService';

const baseUrl = '/backofficeapi/files';

class UploadService {
  static create(params) {
    const bodyFormData = new FormData();
    bodyFormData.append('file', params.rawFile[0]);
    return http.post(`${baseUrl}/upload/`, bodyFormData, {
      headers: {
        'Content-Type': 'multipart/form-data',
      },
    });
  }

  static get(name) {
    return http.get(`/api/${baseUrl}/${name}`, {
      responseType: 'blob',
    });
  }

  static getMountedUrl(name) {
    return `https://api.dev.k8s.cardhub.com.br${baseUrl}/${name}`;
  }
}

export default UploadService;
