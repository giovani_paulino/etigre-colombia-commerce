import http from 'Arch/services/HttpService';

const recoverUrl = '/api/account/users/backoffice/password-recover';
const changeUrl = '/api/account/users/backoffice/password';

class RecoverService {
  static recover(data) {
    return http.post(`${recoverUrl}`, data);
  }

  static change(data) {
    return http.patch(`${changeUrl}`, data);
  }
}

export default RecoverService;
