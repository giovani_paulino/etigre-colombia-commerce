import RouterMixin from 'Arch/mixins/RouterMixin';
import NotificationMixin from 'Arch/mixins/NotificationMixin';

export default {
  mixins: [NotificationMixin, RouterMixin],
  data: () => ({
    showSuccessToastMessage: true,
    successToasMessageCreate: 'Os dados foram cadastrados.',
    formsRef: 'form',
    formValid: false,
    loadEntityOnCreate: true,
    loadingForm: false,
  }),
  created() {
    if (this.loadEntityOnCreate && this.entityId) {
      this.loadEntityForEdit();
    }
  },
  computed: {
    entityId() {
      return this.lockGetIdParams ? this.staticId : this.$route.params.id;
    },
  },
  methods: {
    loadEntityForEdit() {
      this.requestClass.get(this.entityId)
        .then(({ data }) => this.setEntityData(data))
        .catch((error) => {
          console.log(error);
        });
    },
    setEntityData(data) {
      this.payload = data;
    },
    validateForm() {
      return this.$refs[this.formsRef].validate();
    },
    save() {
      if (!this.validateForm()) {
        this.showErrorToast({
          title: 'Ocorreu um erro!',
          text: 'Preencha os dados corretamente.',
        });
        return;
      }
      if (this.entityId) {
        this.update();
      } else {
        this.create();
      }
    },
    getCreatePayload() {
      return this.payload;
    },
    create() {
      this.loadingForm = true;
      if (this.onSaveExtraClass) {
        this.onSaveExtraClass();
      }
      this.requestClass.save(this.getCreatePayload()).then(() => {
        if (this.showSuccessToastMessage) {
          this.showSuccessToast({
            title: 'Sucesso!',
            text: this.successToasMessageCreate,
          });
        }
        this.afterSave();
      }).catch(({ response }) => {
        this.showMessageError(response);
      }).finally(() => {
        this.loadingForm = false;
      });
    },
    afterSave() {
      if (this.navigateToPageAfterSave) {
        this.navigateTo(this.navigateToPageAfterSave, this.navigateBackParams);
      }
    },
    getEditPayload() {
      return this.payload;
    },
    showMessageError(response) {
      const { data } = response;
      this.showErrorToast({
        text: data.message || 'Falha ao se comunicar com o servidor.',
      });
    },
    update() {
      this.loadingForm = true;
      this.requestClass.edit(this.entityId, this.getEditPayload()).then(() => {
        this.showSuccessToast({
          text: 'Os dados foram atualizados.',
        });

        this.afterSave();
      }).catch(({ response }) => {
        this.showMessageError(response);
      }).finally(() => {
        this.loadingForm = false;
      });
    },
  },
};
