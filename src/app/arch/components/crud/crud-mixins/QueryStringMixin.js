export default {
  data: () => ({
    rows: [],
    selectedRows: [],
    totalItens: 0,
    disableSetQueryString: false,
    queryPaginate: {
      page: 1,
      limit: 15,
      sortBy: undefined,
      sortDir: undefined,
    },
    errors: [],
  }),
  created() {
    if (this.disableSetQueryString) {
      this.getData();
    }
  },
  methods: {
    updateQueryPaginate(params) {
      this.queryPaginate.page = params.page;
      this.queryPaginate.limit = params.limit;
      this.queryPaginate.sortBy = params.sortBy;
      this.queryPaginate.sortDir = params.sortDir;
    },
    getDefaultQueryPage() {
      const defaultPage = 1;
      if (this.disableSetQueryString) {
        return this.queryPaginate.page || defaultPage;
      }
      return this.$route.query.page || defaultPage;
    },
    getDefaultQueryLimit() {
      const defaultLimit = 15;
      if (this.disableSetQueryString) {
        return this.queryPaginate.limit || defaultLimit;
      }
      return this.$route.query.limit || defaultLimit;
    },
    getDefaultQuerySortBy() {
      const defaultSortBy = undefined;
      if (this.disableSetQueryString) {
        return this.queryPaginate.sortBy || defaultSortBy;
      }
      return this.$route.query.sortBy || defaultSortBy;
    },
    getDefaultQuerysortDir() {
      const defaultsortDir = undefined;
      if (this.disableSetQueryString) {
        return this.queryPaginate.sortDir || defaultsortDir;
      }
      return this.$route.query.sortDir || defaultsortDir;
    },
    setQueryStringPaginate({
      page = this.getDefaultQueryPage(),
      limit = this.getDefaultQueryLimit(),
      sortBy = this.getDefaultQuerySortBy(),
      sortDir = this.getDefaultQuerysortDir(),
    } = {}) {
      this.updateQueryPaginate({
        page, limit, sortBy, sortDir,
      });

      if (this.disableSetQueryString) {
        this.getData();
        return;
      }

      this.$router.replace({
        query: {
          ...this.$route.query,
          limit,
          page,
          sortBy: sortBy || undefined,
          sortDir: sortDir || undefined,
        },
      }).catch(() => {});
    },
    pageUpdated(newPage) {
      this.setQueryStringPaginate({ page: newPage });
    },
    rowsPerPageUpdated(newLimit) {
      this.setQueryStringPaginate({ limit: newLimit, page: 1 });
    },
    orderUpdated(event) {
      const { column, sortDir } = event;
      if (sortDir === null) {
        this.setQueryStringPaginate({ sortBy: null, sortDir: null, page: 1 });
        return;
      }
      this.setQueryStringPaginate({ sortBy: column, sortDir, page: 1 });
    },
  },
  watch: {
    totalItens: {
      handler(value) {
        this.$emit('total-itens-updated', value);
      },
    },
    rows: {
      deep: true,
      handler(value) {
        this.$emit('total-rows-updated', value.length);
      },
    },
    '$route.query': {
      immediate: true,
      handler() {
        if (!this.disableSetQueryString) {
          this.getData();
        }
      },
    },
  },
};
