import RouterMixin from 'Arch/mixins/RouterMixin';

export default {
  mixins: [RouterMixin],
  created() {
    this.updatePayloadWithQueryString();
  },
  methods: {
    updatePayloadWithQueryString() {
      const { query } = this.$route;

      const keys = Object.keys(this.payload);
      keys.forEach((key) => {
        this.payload[key] = query[key] || null;
      });
    },
    filter() {
      const filterPayload = this.cleanupPaylod();
      this.updateQueryParams(filterPayload);
    },
    cleanupPaylod() {
      const keys = Object.keys(this.payload);
      const filterPayload = {};
      keys.forEach((key) => {
        filterPayload[key] = this.payload[key] || undefined;
      });
      return filterPayload;
    },
  },
};
