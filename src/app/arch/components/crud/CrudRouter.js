function buildListRoute({
  basePath,
  baseName,
  moduleActive,
  subModuleActive,
  baseBreadcrumbs,
  card,
  listRoute,
}) {
  return {
    path: basePath,
    name: `${baseName}.list`,
    component: () => import('Arch/components/crud/CrudListTemplate.vue'),
    meta: {
      moduleActive,
      subModuleActive,
      breadcrumbs: baseBreadcrumbs,
      card,
      crud: listRoute,
    },
  };
}

function buildNewRoute({
  basePath,
  baseName,
  moduleActive,
  subModuleActive,
  baseBreadcrumbs,
  newRoute,
}) {
  return {
    path: `${basePath}/new`,
    name: `${baseName}.new`,
    component: newRoute.component,
    meta: {
      moduleActive,
      subModuleActive,
      breadcrumbs: [
        ...baseBreadcrumbs,
        ...newRoute.breadcrumbs,
      ],
    },
  };
}
function buildEditRoute({
  basePath,
  baseName,
  moduleActive,
  subModuleActive,
  baseBreadcrumbs,
  editRoute,
}) {
  return {
    path: `${basePath}/:id`,
    name: `${baseName}.edit`,
    component: editRoute.component,
    meta: {
      moduleActive,
      subModuleActive,
      breadcrumbs: [
        ...baseBreadcrumbs,
        ...editRoute.breadcrumbs,
      ],
    },
  };
}

export function buildCrudRoutes({
  basePath,
  baseName,
  moduleActive,
  subModuleActive,
  baseBreadcrumbs,
  card,
  listRoute,
  newRoute,
  editRoute,
}) {
  const routes = [];
  if (listRoute) {
    routes.push(buildListRoute({
      basePath,
      baseName,
      moduleActive,
      subModuleActive,
      baseBreadcrumbs,
      card,
      listRoute,
    }));
  }
  if (newRoute) {
    routes.push(buildNewRoute({
      basePath,
      baseName,
      moduleActive,
      subModuleActive,
      baseBreadcrumbs,
      newRoute,
    }));
  }
  if (editRoute) {
    routes.push(buildEditRoute({
      basePath,
      baseName,
      moduleActive,
      subModuleActive,
      baseBreadcrumbs,
      editRoute,
    }));
  }

  return routes;
}

export function dois() {}
