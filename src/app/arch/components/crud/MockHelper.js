/* istanbul ignore file */
class MockHelper {
  static mockPromiseListRequest(resolveData, total = null) {
    return new Promise((resolve, reject) => {
      resolve({
        data: {
          content: resolveData,
          total: total || resolveData.length,
        },
      });
      reject();
    });
  }

  static mockPromiseGetRequest(resolveData) {
    return new Promise((resolve, reject) => {
      resolve({
        data: {
          ...resolveData,
        },
      });
      reject();
    });
  }

  static mockPromiseSaveEditOrRemoveRequest() {
    return new Promise((resolve, reject) => {
      resolve();
      reject();
    });
  }

  static mockPromiseGetFileRequest() {
    return new Promise((resolve, reject) => {
      const blob = new Blob([''], { type: 'text/html' });
      blob.lastModifiedDate = '';
      blob.name = 'filename';
      resolve(blob);
      reject();
    });
  }

  static replicate(base, times) {
    const retorno = [];

    for (let index = 1; index < times + 1; index += 1) {
      retorno.push({
        ...base,
        id: index,
      });
    }

    return retorno;
  }
}

export default MockHelper;
