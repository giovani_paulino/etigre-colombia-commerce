export default {
  inheritAttrs: false,
  computed: {
    getValue: {
      get() {
        return this.value;
      },
      set(value) {
        this.$emit('input', value);
      },
    },
  },
  props: {
    value: {
      type: [String, Number],
      required: true,
    },
    outlined: {
      type: Boolean,
      default: true,
    },
    color: {
      type: String,
      default: '#202433',
    },
    id: {
      type: String,
      default() {
        return `input:${this.$attrs.label}`;
      },
    },
  },
};
