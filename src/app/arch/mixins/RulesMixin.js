import moment from 'moment';

const DDMMYYYY = 'DD/MM/YYYY';
const MAXPASSWORD = 20;
const MINPASSWORD = 8;
const YYYYMMDD = 'YYYY-MM-DD';

const exactLengthErrorMessage = 'rules.exactLengthErrorMessage';
const finalDateGreaterThanInitialDateErrorMessage = 'rules.finalDateGreaterThanInitialDateErrorMessage';
const finalDateMaximumThanInitialDateErrorMessage = 'rules.finalDateMaximumThanInitialDateErrorMessage';
const finalValueGreaterThanInitialValueErrorMessage = 'rules.finalValueGreaterThanInitialValueErrorMessage';
const initialDateLessThanFinalDateErrorMessage = 'rules.initialDateLessThanFinalDateErrorMessage';
const initialDateMinimumThanFinalDateErrorMessage = 'rules.initialDateMinimumThanFinalDateErrorMessage';
const initialValueLessThanFinalValueErrorMessage = 'rules.initialValueLessThanFinalValueErrorMessage';
const invalidDateErrorMessage = 'rules.invalidDateErrorMessage';
const maxLengthErrorMessage = 'rules.maxLengthErrorMessage';
const maxPercentageLengthErrorMessage = 'rules.maxPercentageLengthErrorMessage';
const minLengthErrorMessage = 'rules.minLengthErrorMessage';
const onlyAlphabeticCharactersErrorMessage = 'rules.onlyAlphabeticCharactersErrorMessage';
const onlyAsciiCharactersAllowedErrorMessage = 'rules.onlyAsciiCharactersAllowedErrorMessage';
const requiredErrorMessage = 'rules.requiredErrorMessage';
const specialCharacterRequiredErrorMessagewLocal = 'rules.specialCharacterRequiredErrorMessagewLocal';
const onlyAlphaNumericCharactersErrorMessage = 'rules.onlyAlphaNumericCharactersErrorMessage';

export default {
  computed: {
    isEmpty() {
      return (v) => {
        if (typeof v === 'boolean') {
          return true;
        }
        if (!v) return true;
        if (v.length === 0) {
          return this.$t(requiredErrorMessage);
        }
        return true;
      };
    },
    requiredRule() {
      return (v) => {
        if (typeof v === 'boolean') {
          return true;
        }
        return !!v || this.$t(requiredErrorMessage);
      };
    },
    strongPassword() {
      return (v) => {
        const regExStrongPassword = /^.*\W.*$/gm;
        return regExStrongPassword.test(v) || this.$t(specialCharacterRequiredErrorMessagewLocal);
      };
    },
    equalsToField() {
      return (v, comparisonField, errorMessage) => {
        if (!comparisonField || !v) return true;

        if (comparisonField !== v) {
          return this.$t(errorMessage);
        }
        return true;
      };
    },
    differsFromField() {
      return (v, comparisonField, errorMessage) => {
        if (!comparisonField || !v) return true;

        if (comparisonField === v) {
          return this.$t(errorMessage);
        }
        return true;
      };
    },
    passwordMinLength() {
      return (v) => !v
        || v.length >= MINPASSWORD
        || this.$t(minLengthErrorMessage, { length: MINPASSWORD });
    },
    passwordMaxLength() {
      return (v) => !v
        || v.length <= MAXPASSWORD
        || this.$t(maxLengthErrorMessage, { length: MAXPASSWORD });
    },
  },
  methods: {
    maxNumberLength(max) {
      return (v) => !v || v.toString().replace(/[^0-9]+/g, '').length <= max || this.$t(maxLengthErrorMessage);
    },
    maxLength(max) {
      return (v) => !v || v.length <= max || this.$t(maxLengthErrorMessage, { length: max });
    },
    minLength(min) {
      return (v) => !v || v.length >= min || this.$t(minLengthErrorMessage, { length: min });
    },
    exactLength(length) {
      return (v) => !v || v.length === length || this.$t(exactLengthErrorMessage);
    },
    exactNumberLength(length) {
      return (v) => !v || v.toString().replace(/[^0-9]+/g, '').length === length || this.$t(exactLengthErrorMessage);
    },
    maxPercentageLength(max) {
      return (v) => !v || Number(v.toString().replace(/[^0-9]+/g, '').replace(',', '.')) <= max || `${this.$t(maxPercentageLengthErrorMessage)} ${max}%`;
    },
    requiredRuleObject(test) {
      return (v) => {
        if (typeof v === 'undefined') {
          return this.$t(requiredErrorMessage);
        }
        if (typeof v[test] === 'boolean') {
          return true;
        }
        return !!v[test] || this.$t(requiredErrorMessage);
      };
    },
    requiredGiven(test) {
      return (v) => (typeof v === 'boolean' || !!v || !test) || this.$t(requiredErrorMessage);
    },
    dateGreaterThan(comparison, options = {
      valueDateFormat: DDMMYYYY,
      errorMessage: finalDateGreaterThanInitialDateErrorMessage,
    }) {
      return (v) => {
        if (!comparison || !v) return true;
        if (v.length !== DDMMYYYY.length) return this.$t(invalidDateErrorMessage);
        const comparisonDate = moment(comparison, YYYYMMDD).format(YYYYMMDD);
        const dateTransformation = moment(v, options.valueDateFormat || DDMMYYYY).format(YYYYMMDD);
        if (moment(dateTransformation).isBefore(comparisonDate)) {
          return this.$t(options.errorMessage || finalDateGreaterThanInitialDateErrorMessage);
        }
        return true;
      };
    },
    onlyAlphabeticCharacters() {
      return (v) => {
        const regex = /^[A-Za-zÀ-ÖØ-öø-ÿ ]+$/gi;
        return regex.test(v) || this.$t(onlyAlphabeticCharactersErrorMessage);
      };
    },
    dateMaximumThan(comparison, maximum) {
      return (v) => {
        if (!comparison || !v) return true;
        const comparisonDate = moment(comparison, YYYYMMDD).add(maximum, 'day').format(YYYYMMDD);
        const dateTransformation = v.split('/').reverse().join('-');
        if (moment(dateTransformation).isAfter(comparisonDate)) {
          return this.$t(finalDateMaximumThanInitialDateErrorMessage, { days: maximum });
        }
        return true;
      };
    },
    dateLesserThan(comparison, options = {
      valueDateFormat: DDMMYYYY,
      errorMessage: initialDateLessThanFinalDateErrorMessage,
    }) {
      return (v) => {
        if (!comparison || !v) return true;
        if (v.length !== DDMMYYYY.length) return this.$t(invalidDateErrorMessage);
        const comparisonDate = moment(comparison, YYYYMMDD).format(YYYYMMDD);
        const dateTransformation = moment(v, options.valueDateFormat || DDMMYYYY).format(YYYYMMDD);
        if (moment(dateTransformation).isAfter(comparisonDate)) {
          return this.$t(options.errorMessage || initialDateLessThanFinalDateErrorMessage);
        }
        return true;
      };
    },
    dateMinimumThan(comparison, minimum) {
      return (v) => {
        if (!comparison || !v) return true;
        const comparisonDate = moment(comparison, YYYYMMDD).subtract(minimum, 'day').format(YYYYMMDD);
        const dateTransformation = v.split('/').reverse().join('-');
        if (moment(dateTransformation).isBefore(comparisonDate)) {
          return this.$t(initialDateMinimumThanFinalDateErrorMessage, { days: minimum });
        }
        return true;
      };
    },
    onlyAsciiCharacters() {
      return (v) => new RegExp(/^[.,!?0-9a-záàâãéèêíïóôõöúçñ ]+$/i).test(v)
        || this.$t(onlyAsciiCharactersAllowedErrorMessage);
    },
    valueLessThan(comparison) {
      return (value) => {
        if (!comparison || !value) return true;
        const floatValue = this.parseValueToFloat(`${value}`);
        if (floatValue > comparison) {
          return this.$t(initialValueLessThanFinalValueErrorMessage);
        }
        return true;
      };
    },
    valueGreaterThan(comparison) {
      return (value) => {
        if (!comparison || !value) return true;
        const floatValue = this.parseValueToFloat(`${value}`);
        if (floatValue < this.parseValueToFloat(`${comparison}`)) {
          return this.$t(finalValueGreaterThanInitialValueErrorMessage);
        }
        return true;
      };
    },
    parseValueToFloat(value) {
      return parseFloat(value.replace(/[.R$]/gi, '').replace(',', '.').trim());
    },
    onlyAlphaNumericCharacters() {
      return (v) => new RegExp('^[A-Za-z0-9]+$', 'gi').test(v) || this.$t(onlyAlphaNumericCharactersErrorMessage);
    },
  },
};
