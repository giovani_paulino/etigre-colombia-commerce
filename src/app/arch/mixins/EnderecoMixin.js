import NotificationMixin from './NotificationMixin';

export default {
  name: 'EnderecoMixin',
  mixins: [NotificationMixin],
  data: () => ({
    inputsDisabled: true,
    logradEndList: [],
    bairroEndList: [],
  }),
  computed: {
    getCep() {
      return true;
    },
    getLoradPlaceholder() {
      return !this.inputsDisabled ? 'Digite o nome da rua' : null;
    },
    getBairroPlaceholder() {
      return !this.inputsDisabled ? 'Digite o nome do bairro' : null;
    },
  },
  methods: {
    serchCep() {
      this.makeRequest();
    },
    makeRequest() {
      this.requestClass.getEndereco(this.getCep)
        .then(({ data }) => {
          this.handleDataRequest(data);
        }).catch(({ response }) => {
          this.showMessageError(response);
        });
    },
    handleDataRequest(data) {
      this.checkDataContent(data);
    },
    checkDataContent(data) {
      if (data.total > 0) {
        this.clearInputs();
        return this.ckeckDataLength(data);
      }
      return this.showErrorToast({
        text: 'Endereço não encontrado',
      });
    },
    clearInputs() {
      Object.keys(this.getInputsByRef())
        .forEach((key) => {
          this.handleClearInput(key);
        });
    },
    getInputsByRef(payload) {
      return payload;
    },
    ckeckDataLength(data) {
      if (data.total > 1) {
        return this.handleManualList(data.data);
      }
      return this.handleSetPayload(data.data);
    },
    handleManualList(data) {
      this.handleDisplayList();
      this.handleDataList(data);
      this.handleStateAndUf(data[0]);
    },
    handleDisplayList(value = false) {
      this.inputsDisabled = value;
    },
    handleDataList(data = []) {
      this.logradEndList = this.mapKey(data, 'logradouro');
      this.bairroEndList = this.mapKey(data, 'bairro');
    },
    handleStateAndUf(data) {
      return data;
    },
    mapKey(list = [], key) {
      return list.map((item) => item[key]);
    },
    handleSetPayload(data) {
      this.handleDisplayList(true);
      this.setPayload(data[0]);
    },
    setPayload(data) {
      this.handleEndereco(data);
      this.handleStateAndUf(data);
    },
    handleEndereco(data) {
      return data;
    },
    showMessageError(response) {
      const { data } = response;
      this.showErrorToast({
        text: data.message || 'Falha ao se comunicar com o servidor.',
      });
    },
  },
};
