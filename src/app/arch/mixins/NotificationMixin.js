const dialogContentClass = 'dialog-content';
const dialogConfirmButtonClass = 'dialog-confirm-button-class';
const swalClass = {
  title: 'dialog-ok-title',
  content: dialogContentClass,
  confirmButton: dialogConfirmButtonClass,
};

const defaultToastProps = {
  showConfirmButton: false,
  timer: 3000,
  timerProgressBar: true,
  position: 'top-end',
  toast: true,
};

function genIconHtml(icon) {
  return `<i aria-hidden="true" class="v-icon notranslate mdi ${icon} theme--light" style="color: #fff; caret-color: #fff;"></i>`;
}

function genHtml(title, text) {
  return `
        <div style="text-align: left; margin-left: 15px; color: #fff">
          <strong>${title}</strong><br>
          ${text}
        </div>`;
}

export default {
  methods: {
    showWarningToast({
      title,
      text,
    }) {
      return this.$swal({
        ...defaultToastProps,
        html: genHtml(title, text),
        iconHtml: genIconHtml('mdi-alert'),
        icon: 'warning',
        customClass: {
          ...swalClass,
          container: 'dialog-warning-container',
        },
      });
    },
    showErrorToast({
      title = 'Ocorreu um erro!',
      text,
    }) {
      return this.$swal({
        ...defaultToastProps,
        html: genHtml(title, text),
        iconHtml: genIconHtml('mdi-close-circle'),
        icon: 'error',
        customClass: {
          ...swalClass,
          container: 'dialog-error-container',
        },
      });
    },
    showSuccessToast({
      title = 'Sucesso!',
      text,
    }) {
      return this.$swal({
        ...defaultToastProps,
        html: genHtml(title, text),
        iconHtml: genIconHtml('mdi-check-circle'),
        icon: 'success',
        customClass: {
          ...swalClass,
          container: 'dialog-success-container',
        },
      });
    },
    showOkDialog({
      title,
      text,
      confirmButtonText = 'OK',
      customClassTitle = 'dialog-confirm-title',
      contentClass = dialogContentClass,
      headerClass = 'dialog-header',
      actionsClass = 'dialog-actions',
      width = '27em',
      confirmButtonColor = '#FFFFFF',
    }) {
      return this.$swal({
        title,
        text,
        confirmButtonText,
        confirmButtonColor,
        customClassTitle,
        contentClass,
        headerClass,
        actionsClass,
        width,
        customClass: {
          ...swalClass,
          container: 'dialog-ok-container',
        },
      });
    },
    showConfirmDeleteDialog({
      title,
      text,
    }) {
      return this.showConfirmDialog({
        title,
        text,
      });
    },
    showConfirmDialog({
      title,
      text,
      confirmButtonText = 'CONFIRMAR',
      cancelButtonText = 'CANCELAR',
      confirmButtonColor = '#FFFFFF',
      cancelButtonColor = '#FFFFFF',
      customClassTitle = 'dialog-confirm-title',
      contentClass = dialogContentClass,
      headerClass = 'dialog-header',
      actionsClass = 'dialog-actions',
      input,
      inputPlaceholder,
      inputValidator,
      width = '27em',
    }) {
      return this.$swal({
        title,
        text,
        confirmButtonText,
        cancelButtonText,
        confirmButtonColor,
        cancelButtonColor,
        showCancelButton: true,
        reverseButtons: true,
        allowOutsideClick: false,
        width,
        input,
        inputPlaceholder,
        inputValidator,
        customClass: {
          title: customClassTitle,
          header: headerClass,
          content: contentClass,
          actions: actionsClass,
          confirmButton: dialogConfirmButtonClass,
          cancelButton: 'dialog-cancel-button-class',
          container: 'dialog-confirm-container',
        },
      });
    },
  },
};
