export default {
  methods: {
    navigateTo(routeName, params = {}) {
      return this.$router.push({ name: routeName, params });
    },
    updateQueryParams(params) {
      this.$route.query.page = 1;
      this.$router.push({
        query: {
          ...this.$route.query,
          ...params,
        },
      }).catch(() => {});
    },
    getQueryParams(defaultParams = {
      limit: 15,
      page: 1,
    }) {
      if (this.disableSetQueryString) {
        return {
          ...defaultParams,
          ...this.queryPaginate,
        };
      }
      return {
        ...defaultParams,
        ...this.$route.query,
      };
    },
  },
};
