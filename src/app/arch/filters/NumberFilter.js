function NumberFilter(number) {
  return number
    .toString()
    .split(/(?=(?:\d{3})+(?:,|$))/g)
    .join('.');
}

export default NumberFilter;
