function CnpjFilter(cnpj) {
  let newCnpj = cnpj;

  if (typeof cnpj !== 'string') {
    newCnpj = cnpj.toString();
  }

  if (!newCnpj || newCnpj.length !== 14) {
    return newCnpj;
  }

  return newCnpj.replace(/(\d{2})(\d{3})(\d{3})(\d{4})(\d{2})/g, '$1.$2.$3/$4-$5');
}

export default CnpjFilter;
