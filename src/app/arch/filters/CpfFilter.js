function CpfFilter(cpf) {
  let newCpf = cpf;

  if (typeof cpf !== 'string') {
    newCpf = cpf.toString();
  }

  if (!newCpf || newCpf.length !== 11) {
    return newCpf;
  }

  return newCpf.replace(/(\d{3})(\d{3})(\d{3})(\d{2})/g, '$1.$2.$3-$4');
}

export default CpfFilter;
