import CrudRequest from 'Arch/components/crud/CrudRequest';

class ClientRequest extends CrudRequest {
  static baseUrl() {
    return '/api/teste';
  }
}

export default ClientRequest;
