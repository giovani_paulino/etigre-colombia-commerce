import { buildCrudRoutes } from '../arch/components/crud/CrudRouter';

const featureBreadcrumb = [
  {
    text: 'Home',
    disabled: false,
    to: 'home',
  },
  {
    text: 'Produtos',
    disabled: false,
    to: 'teste.list',
  },
];

export default [
  ...buildCrudRoutes({
    baseName: 'teste',
    basePath: '/',
    moduleActive: 'teste',
    baseBreadcrumbs: featureBreadcrumb,
    listRoute: {
      navigateBackRoute: 'dashboard',
      pageTitle: 'Dados',
      pageContent: () => import('App/client/ClientCrudList.vue'),
      filterComponent: () => import('@/app/client/components/table/filters/ClientFilter.vue'),
    },
  }),
];
