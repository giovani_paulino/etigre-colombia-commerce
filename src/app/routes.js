import Client from 'App/client/routes';
import Home from 'App/home/routes';

console.log(Home);
export default [
  {
    path: '/',
    component: () => import('Arch/template/Template.vue'),
    children: [
      ...Client,
      ...Home,
    ],
  },
];
